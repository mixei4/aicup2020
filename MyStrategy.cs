using Aicup2020.Model;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Aicup2020
{
    public class MyStrategy
    {
        private bool firstRunEver = true;
        private PlayerView playerView;
        private int[,] reacheabilityMap;
        private int reacheabilityTick = 0;
        private int myId;
        private Player myPlayer;
        private int myPopulation;
        private int myMaxPopulation;
        private int myRealMaxPopulation;
        private int myPotentialResource;
        private Entity[] resourceEntities;
        private List<Entity> ghostResourceEntities = new List<Entity>();
        private Entity[] ghostEnemyEntities = new Entity[0];
        private int ghostCorrection = -2;
        private Entity[] enemyEntities;
        private HashSet<int> enemyEntitiesIds;
        private Entity[] enemyEntitiesLastTick = new Entity[0];
        private Dictionary<int, int> enemyEntitiesHealth;
        private Dictionary<int, Entity> myEntitiesById;
        private List<Dictionary<int, Entity>> myEntitiesByIdHistory = new List<Dictionary<int, Entity>>();
        private Dictionary<EntityType, int> builderIdByType = new Dictionary<EntityType, int>();
        private Dictionary<EntityType, int> repairerIdByType = new Dictionary<EntityType, int>();
        private Action lastAction;
        public static PathMapCell[,] MMM;

        private void InitReacheabilityMap()
        {
            if (playerView.CurrentTick < reacheabilityTick)
            {
                reacheabilityMap = GetNewWorldMap(x => x.Id == 0 || x.EntityType == EntityType.BuilderUnit && !Info.Repairing(x) ? 0 : -1);
                var start = new Vec2Int(10, 10);
                reacheabilityMap[start.X, start.Y] = 1;
                RunWave(ref reacheabilityMap, start, x => false);
            }
        }

        private void InitOneTime()
        {
            Info.VecInf = new Vec2Int(playerView.MapSize - 1, playerView.MapSize - 1);
            Info.Vec8 = Info.Vec4.Concat(Info.Vec4x).ToArray();
            Info.RepairThreshold = playerView.EntityProperties[EntityType.RangedUnit].Attack.Value.Damage - 1;
            if (playerView.Players.Length < 4)
            {
                Info.Round = 3;
            }
            else if (playerView.FogOfWar)
            {
                Info.Round = 2;
            }
            else
            {
                Info.Round = 1;
            }
            System.Console.WriteLine(Info.Round.ToString());
            foreach (EntityType type in System.Enum.GetValues(typeof(EntityType)))
            {
                builderIdByType.Add(type, 0);
                repairerIdByType.Add(type, 0);
            }
            reacheabilityTick = Info.Round > 1 ? 350 : 0;
            if (Info.Round > 1)
            {
                for (int i = 0; i < 40; i++)
                {
                    for (int j = 0; j < 40; j++)
                    {
                        if (i > 15 || j > 15)
                        {
                            var entity = new Entity(10000000 * 2 + i * 30 + j, 10000001, EntityType.Resource, new Vec2Int(i, j), 1, true);
                            ghostResourceEntities.Add(entity);
                        }
                    }
                }
            }
            Info.NowVisible = new NowVisible();
            Info.OnceVisible = new OnceVisible();
            Info.ResourceMap = new ResourceMap();
            Info.ProjectedMap = new ProjectedMap();
            Info.PathMap = new PathMap();
        }

        private bool ShouldRemoveFromWing(int entityId)
        {
            var result = false;
            if (!myEntitiesById.ContainsKey(entityId))
            {
                result = true;
            }
            else
            {
                var entity = myEntitiesById[entityId];
                var closestEnemy = GetClosestEnemy(entity.Position, new EntityType[] { EntityType.Turret }, true);
                if (closestEnemy.HasValue && closestEnemy.Value.Dist(entity) <= Info.PlayerView.EntityProperties[EntityType.RangedUnit].SightRange)
                {
                    result = true;
                }
            }
            if (result)
            {
                Info.WingDirection.Remove(entityId);
            }
            return result;
        }

        private void SetWings()
        {
            if (Info.Round == 3)
            {
                if (Info.MyEntitiesByType[EntityType.RangedUnit].Length < 10)
                {
                    Info.LeftWing = new HashSet<int>();
                    Info.RightWing = new HashSet<int>();
                    Info.WingDirection = new Dictionary<int, Vec2Int>();
                    return;
                }
                Info.LeftWing.RemoveWhere(x => ShouldRemoveFromWing(x));
                Info.RightWing.RemoveWhere(x => ShouldRemoveFromWing(x));

                var maxCount = System.Math.Min(5, Info.MyEntitiesByType[EntityType.RangedUnit].Length / 4);
                if (Info.LeftWing.Count < maxCount)
                {
                    var potentialWing = Info.MyEntitiesByType[EntityType.RangedUnit].Where(x => !Info.LeftWing.Contains(x.Id) && !Info.RightWing.Contains(x.Id)).OrderBy(x => -x.Id).First();
                    if (!ShouldRemoveFromWing(potentialWing.Id))
                    {
                        Info.LeftWing.Add(potentialWing.Id);
                        Info.WingDirection.Add(potentialWing.Id, new Vec2Int(Info.PlayerView.MapSize / 4, Info.PlayerView.MapSize - 1));
                    }
                }
                if (Info.RightWing.Count < maxCount)
                {
                    var potentialWing = Info.MyEntitiesByType[EntityType.RangedUnit].Where(x => !Info.LeftWing.Contains(x.Id) && !Info.RightWing.Contains(x.Id)).OrderBy(x => -x.Id).First();
                    if (!ShouldRemoveFromWing(potentialWing.Id))
                    {
                        Info.RightWing.Add(potentialWing.Id);
                        Info.WingDirection.Add(potentialWing.Id, new Vec2Int(Info.PlayerView.MapSize - 1, Info.PlayerView.MapSize / 4));
                    }
                }

                foreach (var entityId in Info.LeftWing)
                {
                    if (!Info.WingDirection[entityId].Eq(Info.VecInf) && myEntitiesById[entityId].Position.Y > Info.PlayerView.MapSize - Info.PlayerView.EntityProperties[EntityType.RangedUnit].SightRange)
                    {
                        Info.WingDirection[entityId] = Info.VecInf;
                    }
                }
                foreach (var entityId in Info.RightWing)
                {
                    if (!Info.WingDirection[entityId].Eq(Info.VecInf) && myEntitiesById[entityId].Position.X > Info.PlayerView.MapSize - Info.PlayerView.EntityProperties[EntityType.RangedUnit].SightRange)
                    {
                        Info.WingDirection[entityId] = Info.VecInf;
                    }
                }
            }
        }

        private void Init(PlayerView _playerView, DebugInterface _debugInterface)
        {
            Info.PlayerView = _playerView;
            playerView = _playerView;
            Info.DebugInterface = _debugInterface;
            myId = playerView.MyId;
            myPlayer = playerView.Players.Where(x => x.Id == myId).First();
            if (firstRunEver)
            {
                firstRunEver = false;
                InitOneTime();
            }
            Info.EntitiesIds = playerView.Entities.Select(x => x.Id).ToHashSet();
            Info.EntitiesById = playerView.Entities.ToDictionary(x => x.Id, x => x);
            Info.MyEntities = playerView.Entities.Where(x => x.PlayerId == myId).ToArray();
            resourceEntities = playerView.Entities.Where(x => x.EntityType == EntityType.Resource).ToArray();
            enemyEntities = playerView.Entities.Where(x => x.PlayerId != myId && x.EntityType != EntityType.Resource && x.EntityType != EntityType.Wall).ToArray();
            enemyEntitiesIds = enemyEntities.Select(x => x.Id).ToHashSet();
            enemyEntitiesHealth = enemyEntities.ToDictionary(x => x.Id, x => x.Health);
            Info.precountedCloseEnemiesRange = playerView.EntityProperties[EntityType.RangedUnit].Attack.Value.AttackRange;
            Info.precountedCloseAlliesRange = playerView.EntityProperties[EntityType.RangedUnit].Attack.Value.AttackRange - 1;
            Info.CloseEnemyEntities = new Dictionary<int, Entity[]>(); // This is used to build itself
            Info.CloseEnemyEntities = Info.MyEntities.ToDictionary(x => x.Id, x => GetEnemiesInRange(x, Info.precountedCloseEnemiesRange));
            Info.MyEntities = Info.MyEntities.OrderBy(x => playerView.EntityProperties[x.EntityType].Attack.HasValue ? GetEnemiesInRange(x, playerView.EntityProperties[x.EntityType].Attack.Value.AttackRange).Length : 0).ToArray();
            myEntitiesById = Info.MyEntities.ToDictionary(x => x.Id, x => x);
            myEntitiesByIdHistory.Add(myEntitiesById);
            Info.MyEntitiesByType = new Dictionary<EntityType, Entity[]>();
            foreach (EntityType type in System.Enum.GetValues(typeof(EntityType)))
            {
                Info.MyEntitiesByType[type] = Info.MyEntities.Where(x => x.EntityType == type).ToArray();
            }
            Info.timer[7].Start();
            Info.MyEntitiesIds = Info.MyEntities.Select(x => x.Id).ToHashSet();
            SetWings();
            System.Func<Entity, int> RepairerCount = x => Info.MyEntitiesByType[EntityType.BuilderUnit].Where(y => y.Dist(x) == 1).Count();
            Info.BuildingsRepairersCount = Info.MyEntities.Where(x => x.Health < Info.PlayerView.EntityProperties[x.EntityType].MaxHealth).Where(x => x.EntityType.In(Info.EntityTypesForFastRepair)).Select(x => new System.Tuple<int, int>(x.Id, RepairerCount(x))).Where(x => x.Item2 > 0).ToDictionary(x => x.Item1, x => x.Item2);
            Info.closeAllyEntities = Info.MyEntities.ToDictionary(x => x.Id, x => Info.MyEntitiesByType[x.EntityType].Where(y => y.Id != x.Id).Where(y => y.Dist(x) <= Info.precountedCloseAlliesRange).ToArray());
            Info.timer[7].Stop();
            Info.NowVisible.Update();
            Info.OnceVisible.Update();
            Info.ResourceMap.Update();
            Info.ProjectedMap.Update();
            Info.ForestToCut = new HashSet<Entity>();
            if (Info.Round > 1)
            {
                CheckGhostEnemyEntities();
                enemyEntities = enemyEntities.Concat(ghostEnemyEntities).ToArray();
                resourceEntities = resourceEntities.Concat(ghostResourceEntities).ToArray();
            }
            InitReacheabilityMap();
            Info.timer[5].Start();
            SetAvailableResources();
            Info.timer[5].Stop();
            myPotentialResource = Info.MyEntitiesByType[EntityType.BuilderUnit].Where(x => CanExtractResource(x)).Count();
            myPopulation = Info.MyEntities.Select(x => playerView.EntityProperties[x.EntityType].PopulationUse).Sum();
            myMaxPopulation = Info.MyEntities.Select(x => playerView.EntityProperties[x.EntityType].PopulationProvide).Sum();
            myRealMaxPopulation = Info.MyEntities.Where(x => x.Active).Select(x => playerView.EntityProperties[x.EntityType].PopulationProvide).Sum();
            CheckMyBuilders();
            CheckMyActions();
            Info.TL = Info.timer[9].ElapsedMilliseconds > 32000;
#if DEBUG
            Info.TL = false;
#endif
        }

        private void CheckGhostEnemyEntities()
        {
            if (playerView.CurrentTick < 750 && ghostEnemyEntities.Length == 0 && Info.MyEntitiesByType[EntityType.RangedBase].Length > 0)
            {
                var myPos = Info.MyEntitiesByType[EntityType.RangedBase][0].Position;
                var d = playerView.MapSize - 1 - System.Math.Max(myPos.X, myPos.Y) - 1;
                ghostCorrection = -2;
                var entity = new Entity(10000001, 10000001, EntityType.BuilderUnit, new Vec2Int(myPos.X, myPos.Y + d - 1), 1, true);
                ghostEnemyEntities = new Entity[Info.Round == 2 ? 5 : 3];
                var ind = 0;
                if (Info.Round == 2)
                {
                    ghostEnemyEntities[ind++] = entity;
                    entity.Id++;
                    entity.Position = new Vec2Int(myPos.X + d, myPos.Y);
                    ghostEnemyEntities[ind++] = entity;
                }
                entity.Id++;
                entity.Position = new Vec2Int(playerView.MapSize - 1, playerView.MapSize - 1);
                ghostEnemyEntities[ind++] = entity;
                entity.Id++;
                entity.Position = new Vec2Int(Info.Round == 2 ? 0 : playerView.MapSize / 2, playerView.MapSize - 1);
                ghostEnemyEntities[ind++] = entity;
                entity.Id++;
                entity.Position = new Vec2Int(playerView.MapSize - 1, Info.Round == 2 ? 0 : playerView.MapSize / 2);
                ghostEnemyEntities[ind++] = entity;
            }
            if (playerView.CurrentTick % 10 == 0 && ghostEnemyEntities.Length > 0)
            {
                ghostCorrection = 0 - ghostCorrection;
                ghostEnemyEntities[0].Position = new Vec2Int(ghostEnemyEntities[0].Position.X, System.Math.Clamp(ghostEnemyEntities[0].Position.Y + ghostCorrection, 0, playerView.MapSize - 1));
            }

            var disappearedEnemies = enemyEntitiesLastTick.Where(x => !enemyEntitiesIds.Contains(x.Id));
            ghostEnemyEntities = ghostEnemyEntities.Concat(disappearedEnemies).ToArray();

            var ghostEnemyEntitiesNew = new List<Entity>();
            for (int i = 0; i < ghostEnemyEntities.Length; i++)
            {
                if (!Info.Visible(ghostEnemyEntities[i]))
                {
                    ghostEnemyEntitiesNew.Add(ghostEnemyEntities[i]);
                }
            }
            ghostEnemyEntities = ghostEnemyEntitiesNew.ToArray();

            var ghostResourceEntitiesNew = new List<Entity>();
            for (int i = 0; i < ghostResourceEntities.Count; i++)
            {
                if (!Info.Visible(ghostResourceEntities[i].Position))
                {
                    ghostResourceEntitiesNew.Add(ghostResourceEntities[i]);
                }
            }
            ghostResourceEntities = ghostResourceEntitiesNew;
        }

        private bool ActionIsWrong(int entityId, EntityAction entityAction)
        {
            if (!myEntitiesById.ContainsKey(entityId))
            {
                return true;
            }
            if (entityAction.MoveAction.HasValue)
            {
                var pos = entityAction.MoveAction.Value.Target;
                var entityAtPos = Info.ProjectedMap.GetCell(pos).Entity;
                if (entityAtPos.Id > 0 && playerView.EntityProperties[entityAtPos.EntityType].Size > 1 && entityAtPos.Active)
                {
                    return true;
                }
            }
            if (myEntitiesByIdHistory[playerView.CurrentTick - 3].ContainsKey(entityId))
            {
                var entity = myEntitiesById[entityId];
                if (!Info.Gathering(entity) && !Info.Repairing(entity))
                {
                    if (entity.Position.Eq(myEntitiesByIdHistory[playerView.CurrentTick - 1][entityId].Position) &&
                        entity.Position.Eq(myEntitiesByIdHistory[playerView.CurrentTick - 2][entityId].Position) &&
                        entity.Position.Eq(myEntitiesByIdHistory[playerView.CurrentTick - 3][entityId].Position))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void CheckMyBuilders()
        {
            foreach (var builder in builderIdByType)
            {
                if (!Info.MyEntitiesIds.Contains(builder.Value))
                {
                    builderIdByType[builder.Key] = 0;
                }
            }
            foreach (var repairer in repairerIdByType)
            {
                if (!Info.MyEntitiesIds.Contains(repairer.Value))
                {
                    repairerIdByType[repairer.Key] = 0;
                }
            }
        }

        private void CheckMyActions()
        {
            var actionsToRemove = Info.myActionsById.Where(x => ActionIsWrong(x.Key, x.Value)).Select(x => x.Key);
            foreach (var id in actionsToRemove)
            {
                Info.myActionsById.Remove(id);
            }
        }

        private void Final()
        {
            enemyEntitiesLastTick = playerView.Entities.Where(x => x.PlayerId != myId && x.EntityType != EntityType.Resource && x.EntityType != EntityType.Wall).ToArray();
            if (Info.PlayerView.CurrentTick % 50 == 49 || Info.PlayerView.CurrentTick > 900 && Info.PlayerView.CurrentTick % 10 == 9)
            {
                var s = Info.PlayerView.CurrentTick.ToString() + " - ";
                s += "(" + Info.MyEntitiesByType[EntityType.BuilderUnit].Length.ToString() + "," + Info.MyEntitiesByType[EntityType.RangedUnit].Length.ToString() + ") ";
                for (int i = 0; i < Info.timer.Length; i++)
                {
                    if (Info.timer[i].ElapsedMilliseconds > 0)
                    {
                        s += i.ToString() + "." + Info.timer[i].ElapsedMilliseconds.ToString() + "  ";
                    }
                }
                System.Console.WriteLine(s);
            }
        }

        private bool OutOfBounds(Vec2Int pos)
        {
            return pos.X < 0 || pos.X >= playerView.MapSize || pos.Y < 0 || pos.Y >= playerView.MapSize;
        }

        private bool CanExtractResource(Entity entity)
        {
            foreach (var shift in Info.Vec4)
            {
                var pos = entity.Position.Plus(shift);
                if (!OutOfBounds(pos) && Info.ProjectedMap.GetCell(pos).Entity.EntityType == EntityType.Resource)
                {
                    return true;
                }
            }
            return false;
        }

        private void SetAvailableResources()
        {
            if (playerView.CurrentTick < 450 || playerView.CurrentTick % 4 == 0)
            {
                Info.AvailableResources = new HashSet<Vec2Int>();
                var map = GetNewWorldMap(x => x.Id == 0 || x.EntityType == EntityType.BuilderUnit && !Info.Repairing(x) && !Info.Gathering(x) ? 0 : -1);
                var start = new Vec2Int[] { new Vec2Int(10, 10), new Vec2Int(4, 4) };
                foreach (var pos in start)
                {
                    map[pos.X, pos.Y] = 1;
                }
                RunWave(ref map, start, x => { var cell = Info.ProjectedMap.GetCell(x).Entity; if (cell.EntityType == EntityType.Resource && IsResourceOpen(cell)) { Info.AvailableResources.Add(cell.Position); } return false; });
            }
        }

        private bool IsResourceOpen(Entity resource)
        {
            var closestEnemy = GetClosestEnemy(resource.Position, new EntityType[] { EntityType.RangedUnit, EntityType.MeleeUnit, EntityType.Turret });
            if (closestEnemy.HasValue && resource.Position.Dist(closestEnemy.Value.Position, playerView.EntityProperties[closestEnemy.Value.EntityType].Size) < playerView.EntityProperties[EntityType.RangedUnit].Attack.Value.AttackRange * 2)
            {
                return false;
            }
            foreach (var shift in Info.Vec8)
            {
                var pos = resource.Position.Plus(shift);
                if (!OutOfBounds(pos) && Info.ProjectedMap.GetCell(pos).Entity.EntityType == EntityType.BuilderUnit)
                {
                    return false;
                }
            }
            return true;
        }

        private int[,] GetNewWorldMap(System.Func<Entity, int> CellValue)
        {
            var worldMap = new int[playerView.MapSize, playerView.MapSize];
            for (int i = 0; i < playerView.MapSize; i++)
            {
                for (int j = 0; j < playerView.MapSize; j++)
                {
                    worldMap[i, j] = CellValue(Info.ProjectedMap.map[i, j].Entity);
                }
            }
            return worldMap;
        }

        private Vec2Int RunWave(ref int[,] map, Vec2Int posFrom, System.Func<Vec2Int, bool> ShouldStop, int maxSteps = 100)
        {
            return RunWave(ref map, new Vec2Int[] { posFrom }, ShouldStop, maxSteps);
        }

        private Vec2Int RunWave(ref int[,] map, Vec2Int[] posFrom, System.Func<Vec2Int, bool> ShouldStop, int maxSteps = 100)
        {
            var queue = new Queue<Vec2Int>();
            foreach (var pos in posFrom)
            {
                queue.Enqueue(pos);
            }
            while (queue.Count > 0)
            {
                var pos = queue.Dequeue();
                if (map[pos.X, pos.Y] <= maxSteps)
                {
                    foreach (var d in Info.Vec4)
                    {
                        var posNew = pos.Plus(d);
                        if (!OutOfBounds(posNew))
                        {
                            if (map[posNew.X, posNew.Y] == 0)
                            {
                                map[posNew.X, posNew.Y] = map[pos.X, pos.Y] + 1;
                                queue.Enqueue(posNew);
                            }
                            if (ShouldStop(posNew))
                            {
                                return posNew;
                            }
                        }
                    }
                }
            }
            return Info.VecInf;
        }

        private bool CanReach(Entity entity, Vec2Int pos)
        {
            var map = GetNewWorldMap(x => x.Id == 0 ? 0 : -1);
            map[entity.Position.X, entity.Position.Y] = 1;
            var vecStopped = RunWave(ref map, entity.Position, x => x.Eq(pos));
            return vecStopped.Eq(pos);
        }

        private Vec2Int GetBuildPosition(Entity entity)
        {
            var entityProperties = playerView.EntityProperties[entity.EntityType];
            var availablePositions = new HashSet<Vec2Int>();
            for (int i = entity.Position.X; i < entity.Position.X + entityProperties.Size; i++)
            {
                var pos = new Vec2Int(i, entity.Position.Y - 1);
                if (!OutOfBounds(pos) && Info.ProjectedMap.GetCell(pos).Entity.Id == 0)
                {
                    availablePositions.Add(pos);
                }
                pos = new Vec2Int(i, entity.Position.Y + entityProperties.Size);
                if (!OutOfBounds(pos) && Info.ProjectedMap.GetCell(pos).Entity.Id == 0)
                {
                    availablePositions.Add(pos);
                }
            }
            for (int j = entity.Position.Y; j < entity.Position.Y + entityProperties.Size; j++)
            {
                var pos = new Vec2Int(entity.Position.X - 1, j);
                if (!OutOfBounds(pos) && Info.ProjectedMap.GetCell(pos).Entity.Id == 0)
                {
                    availablePositions.Add(pos);
                }
                pos = new Vec2Int(entity.Position.X + entityProperties.Size, j);
                if (!OutOfBounds(pos) && Info.ProjectedMap.GetCell(pos).Entity.Id == 0)
                {
                    availablePositions.Add(pos);
                }
            }
            if (availablePositions.Count == 0)
            {
                return Info.VecZero;
            }
            if (entity.EntityType == EntityType.MeleeBase || entity.EntityType == EntityType.RangedBase)
            {
                var basePos = Info.Round == 1 ? new Vec2Int(10, 10) : entity.Position;
                var closestEnemy = GetClosestEnemy(basePos);
                if (closestEnemy.HasValue)
                {
                    if (entity.EntityType == EntityType.MeleeBase)
                    {
                        return availablePositions.OrderBy(x => closestEnemy.Value.Dist(x)).First();
                    }
                    else
                    {
                        var range = playerView.EntityProperties[EntityType.RangedUnit].Attack.Value.AttackRange;
                        return availablePositions.OrderBy(x => { var d = closestEnemy.Value.Dist(x); return d <= range ? range - d : d; }).First();
                    }
                }
                else
                {
                    return availablePositions.OrderBy(x => Info.VecInf.Dist(x)).First();
                }
            }
            else if (entity.EntityType == EntityType.BuilderBase)
            {
                var closestEnemy = GetClosestEnemy(GetEntityCenter(entity));
                if (closestEnemy.HasValue && closestEnemy.Value.Dist(GetEntityCenter(entity)) < 20)
                {
                    return availablePositions.OrderBy(x => closestEnemy.Value.Dist(x)).Last();
                }
                else if (Info.AvailableResources.Count > 0)
                {
                    var closestResource = Info.AvailableResources.OrderBy(x => x.Dist(GetEntityCenter(entity))).First();
                    return availablePositions.OrderBy(x => closestResource.Dist(x)).First();
                }
                else if (resourceEntities.Length > 0)
                {
                    var closestResource = resourceEntities.OrderBy(x => x.Dist(GetEntityCenter(entity))).First();
                    return availablePositions.OrderBy(x => closestResource.Dist(x)).First();
                }
                else
                {
                    return availablePositions.OrderBy(x => Info.VecZero.Dist(x)).First();
                }
            }
            return availablePositions.First();
        }

        private bool CanBuild(EntityType entityType, Vec2Int pos, bool now = false)
        {
            var size = playerView.EntityProperties[entityType].Size;
            if (pos.X < 0 || pos.X + size > playerView.MapSize || pos.Y < 0 || pos.Y + size > playerView.MapSize)
            {
                return false;
            }
            var closestEnemy = GetClosestEnemy(pos, new EntityType[] { EntityType.RangedUnit, EntityType.MeleeUnit });
            if (closestEnemy.HasValue && closestEnemy.Value.Dist(pos, size) < 10)
            {
                return false;
            }
            var closestTurret = GetClosestEnemy(pos, new EntityType[] { EntityType.Turret });
            if (closestTurret.HasValue && closestTurret.Value.Dist(pos, size) < 6)
            {
                return false;
            }
            if (playerView.CurrentTick < reacheabilityTick && reacheabilityMap[pos.X, pos.Y] <= 0)
            {
                return false;
            }
            var dX = new Vec2Int(-1, 1);
            var dY = new Vec2Int(-1, 1);
            if (entityType == EntityType.House && pos.X * pos.Y == 0)
            {
                if (pos.X == 0)
                {
                    var p1 = new Vec2Int(pos.X, pos.Y - 1);
                    var p2 = new Vec2Int(pos.X, pos.Y + size);
                    if (OutOfBounds(p1) || Info.ProjectedMap.GetCell(p1).Entity.Id != 0 && playerView.EntityProperties[Info.ProjectedMap.GetCell(p1).Entity.EntityType].Size > 1)
                    {
                        dY.X = 0;
                    }
                    if (OutOfBounds(p2) || Info.ProjectedMap.GetCell(p2).Entity.Id != 0 && playerView.EntityProperties[Info.ProjectedMap.GetCell(p2).Entity.EntityType].Size > 1)
                    {
                        dY.Y = 0;
                    }
                }
                if (pos.Y == 0)
                {
                    var p1 = new Vec2Int(pos.X - 1, pos.Y);
                    var p2 = new Vec2Int(pos.X + size, pos.Y);
                    if (OutOfBounds(p1) || Info.ProjectedMap.GetCell(p1).Entity.Id != 0 && playerView.EntityProperties[Info.ProjectedMap.GetCell(p1).Entity.EntityType].Size > 1)
                    {
                        dX.X = 0;
                    }
                    if (OutOfBounds(p2) || Info.ProjectedMap.GetCell(p2).Entity.Id != 0 && playerView.EntityProperties[Info.ProjectedMap.GetCell(p2).Entity.EntityType].Size > 1)
                    {
                        dX.Y = 0;
                    }
                }
            }
            for (var i = pos.X + dX.X; i < pos.X + size + dX.Y; i++)
            {
                for (var j = pos.Y + dY.X; j < pos.Y + size + dY.Y; j++)
                {
                    if (i < 0 || j < 0 || i >= Info.PlayerView.MapSize || j >= Info.PlayerView.MapSize)
                        continue;
                    var entityAtij = Info.ProjectedMap.map[i, j].Entity;
                    if (entityAtij.Id == 0)
                        continue;
                    if (playerView.EntityProperties[entityAtij.EntityType].Size > 1 && entityAtij.EntityType != EntityType.BuilderBase || (i.BetweenEq(pos.X, pos.X + size - 1) && j.BetweenEq(pos.Y, pos.Y + size - 1)))
                    {
                        return false;
                    }
                    if (Info.Round > 1 && entityType == EntityType.House && (pos.X > 12 || pos.Y > 12) && (i < pos.X && j.BetweenEq(pos.Y, pos.Y + size - 1) || j < pos.Y && i.BetweenEq(pos.X, pos.X + size - 1)) && entityAtij.EntityType == EntityType.Resource)
                    {
                        return false;
                    }
                    if (now && i >= pos.X && i < pos.X + size && j >= pos.Y && j < pos.Y + size && entityAtij.Id > 0)
                    {
                        return false;
                    }    
                }
            }
            if (now)
            {
                var newEntity = new Entity(Info.GetId(), Info.GetId(), entityType, pos, 1, true);
                if (!Info.MyEntitiesByType[EntityType.BuilderUnit].Where(x => !Info.Repairing(x)).Where(x => x.Position.Dist(newEntity) == 1).Any())
                {
                    return false;
                }
            }
            return true;
        }

        private Vec2Int GetBestPosForTurret(bool top)
        {
            var entityType = EntityType.Turret;
            var size = playerView.EntityProperties[entityType].Size;
            var minDist = int.MaxValue;
            Vec2Int bestPos = Info.VecInf;
            for (int i = 5; i < (Info.Round < 3 ? 23 : 28); i += (Info.Round < 3 ? 4 : 5))
            {
                for (int j = 25; j < 30; j += 4)
                {
                    if (Info.Round == 3 && j == 25)
                    {
                        continue;
                    }
                    if (i > 15)
                    {
                        if (Info.Round == 1 || !bestPos.Eq(Info.VecInf))
                        {
                            break;
                        }
                        if (Info.Round < 3 && (i < 20 || j > 25))
                        {
                            continue;
                        }
                    }
                    var pos = new Vec2Int(top ? i : j, top ? j : i);
                    if (CanBuild(entityType, pos))
                    {
                        var buildingCenter = GetFutureEntityCenter(pos, size);
                        var closestBuilder = GetClosestAllyByType(EntityType.BuilderUnit, buildingCenter);
                        var closestEnemy = GetClosestEnemy(buildingCenter, new EntityType[] { EntityType.RangedUnit, EntityType.MeleeUnit });
                        if (closestBuilder.HasValue && closestBuilder.Value.Dist(buildingCenter) < minDist && (!closestEnemy.HasValue || buildingCenter.Dist(closestEnemy.Value.Position) > 20))
                        {
                            minDist = closestBuilder.Value.Dist(buildingCenter);
                            bestPos = pos;
                        }
                    }
                }
            }
            if (Info.Round == 3 && bestPos.Eq(Info.VecInf))
            {
                var points = new Vec2Int[] { new Vec2Int(-1, 1), new Vec2Int(0, 1), new Vec2Int(1, 0), new Vec2Int(1, -1), new Vec2Int(0, -2), new Vec2Int(-1, -2), new Vec2Int(-2, 0), new Vec2Int(-2, -1) };
                foreach (var builderUnit in Info.MyEntitiesByType[EntityType.BuilderUnit].Where(x => x.Position.X > 35 || x.Position.Y > 35).Where(x => x.Position.Between(new Vec2Int(5, 5), Info.VecInf.Minus(new Vec2Int(5, 5)))).OrderBy(x => -System.Math.Max(x.Position.X, x.Position.Y)))
                {
                    var closestTurret = GetClosestAllyByType(EntityType.Turret, builderUnit.Position);
                    if ((!closestTurret.HasValue || builderUnit.Position.Dist(closestTurret.Value) > 10) && Info.closeAllyEntities[builderUnit.Id].Length > 1)
                    {
                        foreach (var d in points)
                        {
                            var pos = builderUnit.Position.Plus(d);
                            if (CanBuild(EntityType.Turret, builderUnit.Position.Plus(d), true))
                            {
                                return pos;
                            }
                        }
                    }
                }
            }
            return bestPos;
        }

        private Vec2Int GetNewBuildingPlace(EntityType entityType)
        {
            if (!GetClosestAllyByType(EntityType.BuilderUnit, Info.VecZero).HasValue)
            {
                return Info.VecInf;
            }
            var size = playerView.EntityProperties[entityType].Size;
            var minDist = int.MaxValue;
            Vec2Int bestPos = Info.VecInf;
            if (entityType == EntityType.Turret)
            {
                var topFirst = Info.Round == 1 || playerView.CurrentTick % 2 == 0;
                bestPos = GetBestPosForTurret(topFirst);
                if (!bestPos.Eq(Info.VecInf))
                {
                    return bestPos;
                }
                if (Info.Round > 1 || Info.MyEntitiesByType[EntityType.Turret].Where(x => x.Position.BetweenEq(new Vec2Int(25, 5), new Vec2Int(30, 15))).Count() < Info.MyEntitiesByType[EntityType.Turret].Where(x => x.Position.BetweenEq(new Vec2Int(5, 25), new Vec2Int(15, 30))).Count())
                {
                    bestPos = GetBestPosForTurret(!topFirst);
                    if (!bestPos.Eq(Info.VecInf))
                    {
                        return bestPos;
                    }
                }
                return Info.VecInf;
            }

            for (int i = 0; i < 23; i++)
            {
                for (int j = 0; j < 23; j++)
                {
                    var pos = new Vec2Int(i, j);
                    if (CanBuild(entityType, pos, true))
                    {
                        return pos;
                    }
                }
            }

            if (Info.PlayerView.CurrentTick > 500)
            {
                for (var line = 32; line < playerView.MapSize - size - 1; line++)
                {
                    for (var i = 0; i <= line; i++)
                    {
                        for (var t = 0; t < 2; t++)
                        {
                            if (t == 0 && i == line)
                            {
                                continue;
                            }
                            var pos = new Vec2Int(t == 0 ? line : i, t == 0 ? i : line);
                            if (CanBuild(entityType, pos, true))
                            {
                                return pos;
                            }
                        }
                    }
                }
            }

            if (entityType == EntityType.House)
            {
                for (int i = 0; i < 20; i+=3)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        var pos = new Vec2Int(j == 0 ? i : 0, j == 0 ? 0 : i);
                        if (CanBuild(entityType, pos))
                        {
                            if (Info.Round > 1)
                            {
                                return pos;
                            }
                            var buildingCenter = GetFutureEntityCenter(pos, size);
                            var closestBuilder = GetClosestAllyByType(EntityType.BuilderUnit, buildingCenter);
                            if (closestBuilder.HasValue && closestBuilder.Value.Dist(buildingCenter) < minDist)
                            {
                                minDist = closestBuilder.Value.Dist(buildingCenter);
                                bestPos = pos;
                            }
                        }
                    }
                }
                if (!bestPos.Eq(Info.VecInf))
                {
                    return bestPos;
                }
            }
            if (Info.Round > 1 && entityType == EntityType.RangedBase)
            {
                for (int i = 9; i < 13; i++)
                {
                    for (int j = 9; j < 13; j++)
                    {
                        var pos = new Vec2Int(i, j);
                        if (CanBuild(entityType, pos))
                        {
                            bestPos = pos;
                            if (bestPos.X - bestPos.Y == 0)
                            {
                                return bestPos;
                            }
                        }
                    }
                }
                if (!bestPos.Eq(Info.VecInf))
                {
                    return bestPos;
                }
            }
            var positionsChecked = 0;
            for (var line = 0; line < playerView.MapSize - size - 1; line++)
            {
                for (var i = 0; i <= line; i++)
                {
                    for (var t = 0; t < 2; t++)
                    {
                        if (t == 0 && i == line)
                        {
                            continue;
                        }
                        var pos = new Vec2Int(t == 0 ? line : i, t == 0 ? i : line);
                        if (CanBuild(entityType, pos))
                        {
                            positionsChecked++;
                            var buildingCenter = GetFutureEntityCenter(pos, size);
                            var closestBuilder = GetClosestAllyByType(EntityType.BuilderUnit, buildingCenter);
                            var d = closestBuilder.Value.Dist(buildingCenter);
                            if (closestBuilder.HasValue && d < minDist)
                            {
                                minDist = d;
                                bestPos = pos;
                            }
                            if (positionsChecked > 5)
                            {
                                return bestPos;
                            }
                        }
                    }
                }
            }
            return bestPos;
        }

        private Entity? GetClosestAllyByTypePrecise(EntityType entityType, Vec2Int pos, bool excludeOccupied = true)
        {
            var map = GetNewWorldMap(x => x.Id == 0 ? 0 : -1);
            map[pos.X, pos.Y] = 1;
            var vecStopped = RunWave(ref map, pos, x => Info.ProjectedMap.GetCell(x).Entity.PlayerId == myPlayer.Id && Info.ProjectedMap.GetCell(x).Entity.EntityType == entityType && (!excludeOccupied || !Info.Occupied(Info.ProjectedMap.GetCell(x).Entity)) && !Info.Repairing(Info.ProjectedMap.GetCell(x).Entity));
            var entity = Info.ProjectedMap.GetCell(vecStopped).Entity;
            if (entity.EntityType == entityType && (!excludeOccupied || !Info.Occupied(entity)) && !Info.Repairing(entity))
            {
                return entity;
            }
            return null;
        }

        private Entity? GetClosestAllyByType(EntityType entityType, Vec2Int pos)
        {
            var sortedEntities = Info.MyEntitiesByType[entityType].OrderBy(x => pos.Dist(x.Position, playerView.EntityProperties[x.EntityType].Size)).ToArray();
            foreach (var entity in sortedEntities)
            {
                var occupied = entity.Id.In(Info.myActionsById.Keys.ToArray());
                var repairing = GetEntitiesCanRepair(entity).Length > 0;
                if (!occupied && !repairing)
                {
                    return entity;
                }
            }
            return null;
        }

        private Entity? GetClosestEnemy(Vec2Int pos, EntityType[] entityTypes = null, bool exclude = false)
        {
            var enemySet = enemyEntities;
            if (entityTypes != null && entityTypes.Length > 0)
            {
                enemySet = enemyEntities.Where(x => exclude ^ x.EntityType.In(entityTypes)).ToArray();
            }
            if (enemySet.Length == 0)
            {
                return null;
            }
            return enemySet.OrderBy(x => pos.Dist(x.Position, playerView.EntityProperties[x.EntityType].Size)).First();            
        }

        private Entity[] GetEnemiesInRange(Entity entity, int range)
        {
            var enemySet = enemyEntities;
            if (range <= Info.precountedCloseEnemiesRange && Info.CloseEnemyEntities.ContainsKey(entity.Id))
            {
                enemySet = Info.CloseEnemyEntities[entity.Id];
            }
            if (enemySet.Length == 0)
            {
                return new Entity[0];
            }
            var result = new HashSet<Entity>();
            var pos = entity.Position;
            for (int i = pos.X; i < pos.X + playerView.EntityProperties[entity.EntityType].Size; i++)
            {
                for (int j = pos.Y; j < pos.Y + playerView.EntityProperties[entity.EntityType].Size; j++)
                {
                    var part = enemySet.Where(x => new Vec2Int(i, j).Dist(x.Position, playerView.EntityProperties[x.EntityType].Size) <= range);
                    result = result.Concat(part).ToHashSet();
                }
            }
            return result.ToArray();
        }

        private Entity[] GetEnemiesCanAttackNow(Entity entity)
        {
            var enemySet = enemyEntities;
            if (Info.CloseEnemyEntities.ContainsKey(entity.Id))
            {
                enemySet = Info.CloseEnemyEntities[entity.Id];
            }
            if (enemySet.Length == 0)
            {
                return new Entity[0];
            }
            var result = new HashSet<Entity>();
            var pos = entity.Position;
            for (int i = pos.X; i < pos.X + playerView.EntityProperties[entity.EntityType].Size; i++)
            {
                for (int j = pos.Y; j < pos.Y + playerView.EntityProperties[entity.EntityType].Size; j++)
                {
                    var part = enemySet.Where(x => playerView.EntityProperties[x.EntityType].Attack.HasValue && new Vec2Int(i, j).Dist(x.Position, playerView.EntityProperties[x.EntityType].Size) <= playerView.EntityProperties[x.EntityType].Attack.Value.AttackRange);
                    result = result.Concat(part).ToHashSet();
                }
            }
            return result.ToArray();
        }

        private bool ShouldBuildBuilding(EntityType baseType)
        {
            if (Info.PlayerView.CurrentTick > Info.PlayerView.MaxTickCount - 35)
            {
                return false;
            }
            if (Info.Round > 1 && playerView.CurrentTick < 170)
            {
                if (myPopulation < 5)
                {
                    return false;
                }
                if (baseType == EntityType.House)
                {
                    var unitsCountShouldBuildSoon = (myMaxPopulation - myPopulation) * 0.6;
                    var unitCost = GetBuildCost(EntityType.BuilderUnit);
                    if (myPlayer.Resource + myPotentialResource > GetBuildCost(baseType) + unitsCountShouldBuildSoon * unitCost)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            if (baseType == EntityType.House)
            {
                if (Info.Round > 1 && myMaxPopulation > 19 && Info.MyEntitiesByType[EntityType.RangedBase].Length == 0 && myPlayer.Resource + myPotentialResource < GetBuildCost(EntityType.RangedBase) + GetBuildCost(EntityType.House))
                {
                    return false;
                }
                var unitsCountShouldBuildSoon = System.Math.Max(0, myMaxPopulation - myPopulation - 9);
                var unitCost = (GetBuildCost(EntityType.BuilderUnit) + GetBuildCost(EntityType.RangedUnit) / 2) + unitsCountShouldBuildSoon / 2;
                return myPopulation == myMaxPopulation ||
                    myPopulation == myRealMaxPopulation && myPlayer.Resource + myPotentialResource > GetBuildCost(baseType) + unitsCountShouldBuildSoon * unitCost ||
                    myMaxPopulation.BetweenEq(20, 65) && myPopulation > myMaxPopulation - 10 ||
                    myMaxPopulation > 65 && myPopulation > myMaxPopulation * 0.9;
            }
            if (baseType == EntityType.BuilderBase)
            {
                return Info.MyEntitiesByType[baseType].Length == 0;
            }
            if (baseType == EntityType.RangedBase)
            {
                return (Info.Round == 1 || myPopulation > 19) && Info.MyEntitiesByType[baseType].Length == 0 && myPlayer.Resource + myPotentialResource > GetBuildCost(baseType) * 0.85;
            }
            if (baseType == EntityType.Turret)
            {
                if (Info.PlayerView.CurrentTick > Info.PlayerView.MaxTickCount - 50)
                {
                    return false;
                }
                return !Info.MyEntitiesByType[baseType].Where(x => !x.Active).Any() && Info.MyEntitiesByType[baseType].Length < (playerView.CurrentTick - 160 - (Info.Round == 1 ? 0 : 50)) / 30;
            }
            return false;
        }

        private bool CanStartBuilding(EntityType entityType, Vec2Int pos, Entity builder)
        {
            var size = playerView.EntityProperties[entityType].Size;
            return (builder.Position.X == pos.X - 1 || builder.Position.X == pos.X + size) && builder.Position.Y.BetweenEq(pos.Y, pos.Y + size - 1) ||
                (builder.Position.Y == pos.Y - 1 || builder.Position.Y == pos.Y + size) && builder.Position.X.BetweenEq(pos.X, pos.X + size - 1);
        }

        private void ResetBuilder(EntityType baseType)
        {
            Info.myActionsById.Remove(builderIdByType[baseType]);
            Info.myActionsById.Remove(repairerIdByType[baseType]);
            builderIdByType[baseType] = 0;
            repairerIdByType[baseType] = 0;
        }

        private Vec2Int GetBestRepairerPosition(Entity repairer, EntityType buildingType, Vec2Int pos)
        {
            var buildingSize = playerView.EntityProperties[buildingType].Size;
            var currentDist = repairer.Dist(pos, buildingSize);
            if (currentDist == 0)
            {
                return new Vec2Int(pos.X + buildingSize / 2, pos.Y + buildingSize);
            }
            if (currentDist == 1)
            {
                return repairer.Position;
            }
            var map = GetNewWorldMap(x => x.Id == 0 ? 0 : -1);
            map[repairer.Position.X, repairer.Position.Y] = 1;
            if (builderIdByType.ContainsKey(buildingType) && Info.myActionsById.ContainsKey(builderIdByType[buildingType]) && Info.myActionsById[builderIdByType[buildingType]].MoveAction.HasValue)
            {
                var builderPos = Info.myActionsById[builderIdByType[buildingType]].MoveAction.Value.Target;
                map[builderPos.X, builderPos.Y] = -1;
            }
            var vecStopped = RunWave(ref map, repairer.Position, x => Info.ProjectedMap.GetCell(x).Entity.Id == 0 && x.Dist(pos, buildingSize) == 1);
            if (vecStopped.Dist(pos, buildingSize) == 1)
            {
                return vecStopped;
            }
            else
            {
                return Info.VecInf;
            }
        }

        private int GetTouchingResourceCount(EntityType entityType, Vec2Int pos, Vec2Int builderPos)
        {
            var size = Info.PlayerView.EntityProperties[entityType].Size;
            var result = 0;
            if (pos.X - builderPos.X == 1 || builderPos.X - pos.X == size)
            {
                for (int j = pos.Y - 1; j < pos.Y + size + 1; j++)
                {
                    var p = new Vec2Int(builderPos.X, j);
                    if (!Info.OutOfBounds(p) && Info.ProjectedMap.GetCell(p).Entity.EntityType == EntityType.Resource)
                    {
                        result++;
                    }
                }
                for (int i = pos.X; i < pos.X + size; i++)
                {
                    var p = new Vec2Int(i, pos.Y - 1);
                    if (!Info.OutOfBounds(p) && Info.ProjectedMap.GetCell(p).Entity.EntityType == EntityType.Resource)
                    {
                        result++;
                    }
                    p = new Vec2Int(i, pos.Y + size);
                    if (!Info.OutOfBounds(p) && Info.ProjectedMap.GetCell(p).Entity.EntityType == EntityType.Resource)
                    {
                        result++;
                    }
                }
            }
            if (pos.Y - builderPos.Y == 1 || builderPos.Y - pos.Y == size)
            {
                for (int i = pos.X - 1; i < pos.X + size + 1; i++)
                {
                    var p = new Vec2Int(i, builderPos.Y);
                    if (!Info.OutOfBounds(p) && Info.ProjectedMap.GetCell(p).Entity.EntityType == EntityType.Resource)
                    {
                        result++;
                    }
                }
                for (int j = pos.Y; j < pos.Y + size; j++)
                {
                    var p = new Vec2Int(pos.X - 1, j);
                    if (!Info.OutOfBounds(p) && Info.ProjectedMap.GetCell(p).Entity.EntityType == EntityType.Resource)
                    {
                        result++;
                    }
                    p = new Vec2Int(pos.X + size, j);
                    if (!Info.OutOfBounds(p) && Info.ProjectedMap.GetCell(p).Entity.EntityType == EntityType.Resource)
                    {
                        result++;
                    }
                }
            }
            return result;
        }

        private void BuildBase(EntityType baseType)
        {
            var builderId = builderIdByType[baseType];
            if (builderId > 0 && myEntitiesById.ContainsKey(builderId) && Info.myActionsById.ContainsKey(builderId))
            {
                if (Info.myActionsById[builderId].MoveAction.HasValue)
                {
                    var moveTarget = Info.myActionsById[builderId].MoveAction.Value.Target;
                    var pos = moveTarget;
                    if (CanStartBuilding(baseType, pos, myEntitiesById[builderId]))
                    {
                        var buildPosChanged = false;
                        if (Info.Round > 1 && baseType == EntityType.RangedBase)
                        {
                            var resourceCount = GetTouchingResourceCount(baseType, pos, myEntitiesById[builderId].Position);
                            if (resourceCount > 0)
                            {
                                var bestPos = pos;
                                foreach (var d in Info.Vec8)
                                {
                                    var newPos = pos.Plus(d);
                                    var newBuilderPos = myEntitiesById[builderId].Position.Plus(d);
                                    if (Info.OutOfBounds(newPos) || Info.OutOfBounds(newBuilderPos))
                                    {
                                        continue;
                                    }
                                    if (Info.ProjectedMap.GetCell(newBuilderPos).Entity.Id == 0 && CanBuild(baseType, newPos))
                                    {
                                        var newResourceCount = GetTouchingResourceCount(baseType, newPos, newBuilderPos);
                                        if (newResourceCount < resourceCount)
                                        {
                                            resourceCount = newResourceCount;
                                            bestPos = newPos;
                                        }
                                    }
                                }
                                if (!bestPos.Eq(pos))
                                {
                                    Info.myActionsById.Remove(builderId);
                                    Info.myActionsById[builderId] = new EntityAction(new MoveAction(bestPos, false, true), null, null, null);
                                    buildPosChanged = true;
                                }
                            }
                        }

                        if (!buildPosChanged || CanStartBuilding(baseType, Info.myActionsById[builderId].MoveAction.Value.Target, myEntitiesById[builderId]))
                        {
                            Info.myActionsById.Remove(builderId);
                            var buildAction = new BuildAction();
                            buildAction.Position = pos;
                            buildAction.EntityType = baseType;
                            Info.myActionsById.Add(builderId, new EntityAction(null, buildAction, null, null));
                        }
                    }
                    else if (!CanBuild(baseType, pos) || !CanReach(myEntitiesById[builderId], pos))
                    {
                        ResetBuilder(baseType);
                        BuildBase(baseType);
                        return;
                    }
                }
                else if (Info.myActionsById[builderId].BuildAction.HasValue)
                {
                    var pos = Info.myActionsById[builderId].BuildAction.Value.Position;
                    var building = Info.ProjectedMap.GetCell(pos).Entity;
                    if (building.Id > 0 && building.EntityType == baseType && !building.Active)
                    {
                        if (repairerIdByType.ContainsKey(baseType) && Info.myActionsById.ContainsKey(repairerIdByType[baseType]) && Info.myActionsById[repairerIdByType[baseType]].MoveAction.HasValue)
                        {
                            if (myEntitiesById[repairerIdByType[baseType]].Dist(building.Position, playerView.EntityProperties[baseType].Size) > 1)
                            {
                                Info.myActionsById.Remove(repairerIdByType[baseType]);
                                var moveAction = new MoveAction();
                                moveAction.Target = GetEntityCenter(building);
                                moveAction.FindClosestPosition = true;
                                moveAction.BreakThrough = true;
                                Info.myActionsById.Add(repairerIdByType[baseType], new EntityAction(moveAction, null, null, null));
                            }
                        }
                        Info.myActionsById.Remove(builderId);
                        builderIdByType[baseType] = 0;
                        repairerIdByType[baseType] = 0;
                        BuildBase(baseType);
                        return;
                    }
                    else if (!CanBuild(baseType, pos))
                    {
                        ResetBuilder(baseType);
                        BuildBase(baseType);
                        return;
                    }
                }
                return;
            }
            ResetBuilder(baseType);
            if (ShouldBuildBuilding(baseType))
            {
                var pos = GetNewBuildingPlace(baseType);
                if (pos.Eq(Info.VecInf) || !CanBuild(baseType, pos))
                {
                    return;
                }
                var buildingSize = playerView.EntityProperties[baseType].Size;
                var buildingCenter = GetFutureEntityCenter(pos, buildingSize);
                var builder = GetClosestAllyByTypePrecise(EntityType.BuilderUnit, buildingCenter, baseType != EntityType.RangedBase);
                if (builder.HasValue)
                {
                    if (baseType == EntityType.RangedBase)
                    {
                        ResetBuilderId(builder.Value.Id);
                    }
                    builderIdByType[baseType] = builder.Value.Id;
                    var moveAction = new MoveAction();
                    moveAction.Target = pos;
                    moveAction.FindClosestPosition = false;
                    moveAction.BreakThrough = false;
                    Info.myActionsById.Add(builderIdByType[baseType], new EntityAction(moveAction, null, null, null));

                    var repairer = GetClosestAllyByTypePrecise(EntityType.BuilderUnit, buildingCenter);
                    if (repairer.HasValue)
                    {
                        var bestRepairerPosition = GetBestRepairerPosition(repairer.Value, baseType, pos);
                        if (!bestRepairerPosition.Eq(Info.VecInf))
                        {
                            repairerIdByType[baseType] = repairer.Value.Id;
                            moveAction = new MoveAction();
                            moveAction.Target = bestRepairerPosition;
                            moveAction.FindClosestPosition = false;
                            moveAction.BreakThrough = false;
                            Info.myActionsById.Add(repairerIdByType[baseType], new EntityAction(moveAction, null, null, null));
                        }
                    }

                    if (Info.Round > 1 && baseType == EntityType.RangedBase)
                    {
                        CancelConstructionInterferingWithRangedBase();
                    }

                    if (CanStartBuilding(baseType, pos, builder.Value))
                    {
                        BuildBase(baseType);
                    }
                }
            }
        }

        private void ResetBuilderId(int builderId)
        {
            foreach (var builder in builderIdByType)
            {
                if (builder.Value == builderId)
                {
                    ResetBuilder(builder.Key);
                }
            }
            foreach (var repairer in repairerIdByType)
            {
                if (repairer.Value == builderId)
                {
                    repairerIdByType[repairer.Key] = 0;
                }
            }
            Info.myActionsById.Remove(builderId);
        }

        private void CancelConstructionInterferingWithRangedBase()
        {
            var rangedBaseBuilder = builderIdByType[EntityType.RangedBase];
            if (Info.MyEntitiesByType[EntityType.RangedBase].Length == 0 && rangedBaseBuilder != 0 && Info.myActionsById.ContainsKey(rangedBaseBuilder))
            {
                var targetRanged = Info.myActionsById[rangedBaseBuilder].MoveAction.HasValue ? Info.myActionsById[rangedBaseBuilder].MoveAction.Value.Target : (Info.myActionsById[rangedBaseBuilder].AttackAction.HasValue ? Info.myActionsById[rangedBaseBuilder].BuildAction.Value.Position : Info.VecInf.Mult(2));
                foreach (var builder in builderIdByType)
                {
                    var builderId = builder.Value;
                    if (builder.Key != EntityType.RangedBase && builderId != 0 && Info.myActionsById.ContainsKey(builderId))
                    {
                        var target = Info.myActionsById[builderId].MoveAction.HasValue ? Info.myActionsById[builderId].MoveAction.Value.Target : (Info.myActionsById[builderId].AttackAction.HasValue ? Info.myActionsById[builderId].BuildAction.Value.Position : Info.VecInf);
                        if (Info.myActionsById.ContainsKey(builderId) && target.Dist(targetRanged) < 12)
                        {
                            ResetBuilder(builder.Key);
                        }
                    }
                }
            }
        }

        private Vec2Int GetEntityCenter(Entity entity)
        {
            var shift = playerView.EntityProperties[entity.EntityType].Size / 2;
            return entity.Position.Plus(new Vec2Int(shift, shift));
        }

        private Vec2Int GetFutureEntityCenter(Vec2Int pos, int size)
        {
            var shift = size / 2;
            return pos.Plus(new Vec2Int(shift, shift));
        }

        private AttackAction GetAttackAction(Entity entity)
        {
            var entityProperties = playerView.EntityProperties[entity.EntityType];
            var result = new AttackAction();
            var enemies = GetEnemiesInRange(entity, entityProperties.Attack.Value.AttackRange);
            if (enemies.Length > 0)
            {
                enemies = enemies.OrderBy(x => System.Math.Abs(x.Health)).ToArray();
                foreach (var enemy in enemies)
                {
                    if (enemyEntitiesHealth[enemy.Id] > 0)
                    {
                        enemyEntitiesHealth[enemy.Id] -= entityProperties.Attack.Value.Damage;
                        result.Target = enemy.Id;
                        return result;
                    }
                    else
                    {
                        entityProperties = playerView.EntityProperties[entity.EntityType];
                    }
                }
                enemyEntitiesHealth[enemies[0].Id] -= entityProperties.Attack.Value.Damage;
                result.Target = enemies[0].Id;
                return result;
            }
            result.Target = null;
            result.AutoAttack = null;
            if (Info.TL && entity.EntityType == EntityType.BuilderUnit)
            {
                var autoAttack = new AutoAttack();
                autoAttack.PathfindRange = 2;
                autoAttack.ValidTargets = new EntityType[1] { EntityType.Resource };
                result.AutoAttack = autoAttack;
            }
            return result;
        }

        private int GetBuildCost(EntityType entityType)
        {
            var result = playerView.EntityProperties[entityType].InitialCost;
            if (playerView.EntityProperties[entityType].CanMove)
            {
                result += Info.MyEntitiesByType[entityType].Length;
            }
            return result;
        }

        private BuildAction GetBuildAction(Entity entity)
        {
            var result = new BuildAction();
            result.Position = Info.VecInf;
            var buildProperties = playerView.EntityProperties[entity.EntityType].Build.Value;
            var buildEntityType = buildProperties.Options[0];
            var shouldHaveResources = builderIdByType.Where(x => x.Value != 0).Select(x => GetBuildCost(x.Key)).Sum() + GetBuildCost(buildEntityType);
            if (myPlayer.Resource + myPotentialResource < shouldHaveResources)
            {
                return result;
            }
            if (Info.Round > 1 && buildEntityType == EntityType.BuilderUnit && Info.AvailableResources.Count == 0)
            {
                return result;
            }
            if (Info.Round == 3 && playerView.CurrentTick < 350 && buildEntityType == EntityType.RangedUnit && Info.MyEntitiesByType[EntityType.RangedUnit].Length >= 18 && !enemyEntities.Where(x => x.Position.X + x.Position.Y < Info.PlayerView.MapSize).Any())
            {
                return result;
            }
            if (buildEntityType == EntityType.RangedUnit)
            {
                result.EntityType = buildEntityType;
                result.Position = GetBuildPosition(entity);
                return result;
            }
            if (buildEntityType == EntityType.BuilderUnit && (Info.MyEntitiesByType[EntityType.RangedBase].Length == 0 || !Info.MyEntitiesByType[EntityType.RangedBase][0].Active))
            {
                result.EntityType = buildEntityType;
                result.Position = GetBuildPosition(entity);
                return result;
            }
            if (Info.Round == 3 && buildEntityType == EntityType.BuilderUnit && Info.MyEntitiesByType[EntityType.BuilderUnit].Length < 90 - (playerView.CurrentTick - 500) / 6 && !enemyEntities.Where(x => x.Position.X + x.Position.Y < Info.PlayerView.MapSize).Any())
            {
                result.EntityType = buildEntityType;
                result.Position = GetBuildPosition(entity);
                return result;
            }
            if (buildEntityType == EntityType.BuilderUnit && Info.MyEntitiesByType[EntityType.BuilderUnit].Length >= 50 + (Info.Round - 1) * 10)
            {
                return result;
            }
            if (buildEntityType == EntityType.BuilderUnit && !enemyEntities.Where(x => x.EntityType.In(new EntityType[] { EntityType.RangedUnit, EntityType.MeleeUnit })).Any())
            {
                result.EntityType = buildEntityType;
                result.Position = GetBuildPosition(entity);
                return result;
            }
            var closestEnemy = GetClosestEnemy(entity.Position);
            if (buildEntityType == EntityType.BuilderUnit && closestEnemy.HasValue && closestEnemy.Value.Position.BetweenEq(Info.VecZero, new Vec2Int(25, 25)))
            {
                return result;
            }
            if (buildEntityType == EntityType.MeleeUnit && (Info.MyEntitiesByType[EntityType.MeleeUnit].Length < 2 || Info.MyEntitiesByType[EntityType.RangedBase].Length == 0))
            {
                result.EntityType = buildEntityType;
                result.Position = GetBuildPosition(entity);
                return result;
            }
            if (playerView.CurrentTick < 111 && buildEntityType == EntityType.BuilderUnit && Info.MyEntitiesByType[EntityType.BuilderUnit].Length < myMaxPopulation - 2)
            {
                result.EntityType = buildEntityType;
                result.Position = GetBuildPosition(entity);
                return result;
            }
            if (playerView.CurrentTick > 200)
            {
                if (buildEntityType == EntityType.BuilderUnit)
                {
                    if (Info.MyEntitiesByType[EntityType.RangedUnit].Length == 0 && Info.MyEntitiesByType[EntityType.BuilderUnit].Length > 0)
                    {
                        return result;
                    }
                    if (Info.MyEntitiesByType[EntityType.BuilderUnit].Length <= Info.MyEntitiesByType[EntityType.RangedUnit].Length)
                    {
                        result.EntityType = buildEntityType;
                        result.Position = GetBuildPosition(entity);
                        return result;
                    }
                }
            }
            if (buildEntityType == EntityType.BuilderUnit && Info.MyEntitiesByType[EntityType.BuilderUnit].Length < myMaxPopulation * (Info.Round == 1 ? 0.5 : 0.6))
            {
                result.EntityType = buildEntityType;
                result.Position = GetBuildPosition(entity);
                return result;
            }
            return result;
        }

        private Entity[] GetEntitiesCanRepair(Entity entity, int distance = 1)
        {
            if (entity.EntityType != EntityType.BuilderUnit)
            {
                return new Entity[0];
            }
            var result = new List<Entity>();
            if (distance == 1)
            {
                foreach (var d in Info.Vec4)
                {
                    var pos = entity.Position.Plus(d);
                    if (Info.OutOfBounds(pos))
                    {
                        continue;
                    }
                    var entityAtPos = Info.ProjectedMap.GetCell(pos).Entity;
                    if (entityAtPos.PlayerId == myPlayer.Id && (!entityAtPos.Active || entityAtPos.Health < playerView.EntityProperties[entityAtPos.EntityType].MaxHealth - Info.RepairThreshold) && entityAtPos.EntityType.In(playerView.EntityProperties[entity.EntityType].Repair.Value.ValidTargets))
                    {
                        result.Add(entityAtPos);
                    }
                }
                return result.OrderBy(x => x.Health).ToArray();
            }
            var map = GetNewWorldMap(x => x.Id == 0 ? 0 : -1);
            map[entity.Position.X, entity.Position.Y] = 1;
            System.Func<Vec2Int, bool> ShouldStopAction = x =>
            {
                var entityAtx = Info.ProjectedMap.GetCell(x).Entity;
                if (entityAtx.PlayerId == myPlayer.Id && (!entityAtx.Active || entityAtx.Health < playerView.EntityProperties[entityAtx.EntityType].MaxHealth - Info.RepairThreshold) && entityAtx.EntityType.In(playerView.EntityProperties[entity.EntityType].Repair.Value.ValidTargets))
                {
                    result.Add(entityAtx);
                }
                return false;
            };
            var vecStopped = RunWave(ref map, entity.Position, ShouldStopAction, distance);
            return result.OrderBy(x => x.Health).ToArray();
        }

        private RepairAction GetRepairAction(Entity entity, ref EntityAction entityAction)
        {
            var result = new RepairAction();
            var buildingsCanRepair = GetEntitiesCanRepair(entity);
            if (buildingsCanRepair.Length > 0)
            {
                result.Target = buildingsCanRepair[0].Id;
                entityAction.AttackAction = null;
            }
            return result;
        }

        private MoveAction GetMoveAction(Entity entity, ref EntityAction entityAction)
        {
            var entityProperties = playerView.EntityProperties[entity.EntityType];
            var result = new MoveAction(entity.Position, true, true);
            var enemiesCanAttackNow = GetEnemiesCanAttackNow(entity);
            var closestEnemy = GetClosestEnemy(entity.Position);
            var bestClosestEnemy = GetClosestEnemy(entity.Position, new EntityType[] { EntityType.Turret }, true);
            var targets = new Vec2Int[2] { new Vec2Int(10, playerView.MapSize - 10), new Vec2Int(playerView.MapSize - 10, 10) };
            if (entity.EntityType == EntityType.MeleeUnit)
            {
                if (bestClosestEnemy.HasValue && enemiesCanAttackNow.Length == 0)
                {
                    result.Target = GetEntityCenter(bestClosestEnemy.Value);
                }
                else if (closestEnemy.HasValue)
                {
                    result.Target = GetEntityCenter(closestEnemy.Value);
                }
                else
                {
                    if (entity.Dist(targets[0]) < entity.Dist(targets[1]))
                    {
                        result.Target = targets[0];
                    }
                    else
                    {
                        result.Target = targets[1];
                    }
                }
            }
            else if (entity.EntityType == EntityType.RangedUnit)
            {
                var dangerousEnemies = GetEnemiesInRange(entity, entityProperties.Attack.Value.AttackRange - 3);
                if (dangerousEnemies.Length > 0 && enemiesCanAttackNow.Length == 0)
                {
                    var closestAlly = Info.MyEntitiesByType[EntityType.RangedUnit].Where(x => entity.Position.Dist(x.Position) <= 1).OrderBy(x => entity.Position.Dist(x.Position)).Skip(1).ToArray();
                    if (!entity.Position.Eq(new Vec2Int(14, 14)))
                    {
                        result.Target = new Vec2Int(14, 14);
                        result.BreakThrough = false;
                        entityAction.AttackAction = null;
                    }
                    return result;
                }
                else if (!Info.TL)
                {
                    var direction = bestClosestEnemy.HasValue ? bestClosestEnemy.Value.Position : (closestEnemy.HasValue ? closestEnemy.Value.Position : Info.VecInf.Mult(2));
                    if (Info.LeftWing.Contains(entity.Id) || Info.RightWing.Contains(entity.Id))
                    {
                        direction = Info.WingDirection[entity.Id];
                    }
                    Info.timer[2].Start();
                    var step = Info.PathMap.GetPath(entity, direction);
                    Info.timer[2].Stop();
                    if (!OutOfBounds(step))
                    {
                        result = new MoveAction(step, false, true);
                        var cell = Info.ProjectedMap.GetCell(step).Entity;
                        if (cell.EntityType == EntityType.Resource || cell.EntityType == EntityType.Wall)
                        {
                            Info.ForestToCut.Add(cell);
                        }
                        return result;
                    }
                }
                else if (Info.Round == 1 && closestEnemy.HasValue && entity.Position.BetweenEq(Info.VecZero, new Vec2Int(20, 20)))
                {
                    closestEnemy = GetClosestEnemy(new Vec2Int(10, 10));
                    result.Target = GetEntityCenter(closestEnemy.Value);
                }
                else if (bestClosestEnemy.HasValue && enemiesCanAttackNow.Length == 0)
                {
                    result.Target = GetEntityCenter(bestClosestEnemy.Value);
                }
                else if (closestEnemy.HasValue)
                {
                    result.Target = GetEntityCenter(closestEnemy.Value);
                }
                else
                {
                    if (entity.Dist(targets[0]) < entity.Dist(targets[1]))
                    {
                        result.Target = targets[0];
                    }
                    else
                    {
                        result.Target = targets[1];
                    }
                }
            }
            else if (entity.EntityType == EntityType.BuilderUnit)
            {
                if (Info.Repairing(entity))
                {
                    var rangedBases = Info.MyEntitiesByType[EntityType.RangedBase].Where(x => x.Dist(entity) == 1);
                    if (rangedBases.Any())
                    {
                        var rangedBase = rangedBases.First();
                        var points = new Vec2Int[] { entity.Position.Plus(new Vec2Int(1, 0)), entity.Position.Plus(new Vec2Int(0, 1)) };
                        foreach (var pos in points)
                        {
                            if (Info.ProjectedMap.GetCell(pos).Entity.Id == 0 && rangedBase.Dist(pos) == 1)
                            {
                                result.Target = pos;
                                entityAction.AttackAction = null;
                                entityAction.RepairAction = null;
                                entityAction.BuildAction = null;
                                return result;
                            }
                        }
                    }
                }
                Entity[] entitiesCanPotentiallyRepair;
                if (!Info.TL && Info.AvailableResources.Count > 0)
                {
                    Info.timer[3].Start();
                    entitiesCanPotentiallyRepair = GetEntitiesCanRepair(entity, 8).ToArray();
                    var repairDist = entitiesCanPotentiallyRepair.Length > 0 ? (Info.MyEntitiesByType[EntityType.RangedBase].Where(x => x.Active).Any() ? 5 : 8) : 1;
                    var step = Info.PathMap.GetPath(entity, Info.VecInf.Mult(2), repairDist);
                    Info.timer[3].Stop();
                    var cell = Info.ProjectedMap.GetCell(step).Entity;
                    if (cell.EntityType == EntityType.Resource && Info.Repairing(entity))
                    {
                        return result;
                    }
                    result.Target = step;
                    if (Info.ProjectedMap.builderDangerMap[entity.Position.X, entity.Position.Y] > 0 && !step.Eq(entity.Position))
                    {
                        entityAction.AttackAction = null;
                        entityAction.RepairAction = null;
                        entityAction.BuildAction = null;
                    }
                    return result;
                }
                else if (Info.Round > 1 && Info.PlayerView.CurrentTick < 350 && Info.AvailableResources.Count == 0 && entity.Position.X > 10 && entity.Position.Y > 10 && !Info.Repairing(entity) && Info.Gathering(entity))
                {
                    var newPos = entity.Position.Plus(entity.Position.X > entity.Position.Y ? new Vec2Int(1, 0) : new Vec2Int(0, 1));
                    var newEntity = new Entity(Info.GetId(), Info.GetId(), EntityType.BuilderUnit, newPos, 1, true);
                    if (!OutOfBounds(newPos) && Info.ProjectedMap.GetCell(newPos).Entity.Id == 0 && Info.Gathering(newEntity))
                    {
                        result.Target = newPos;
                        return result;
                    }
                }
                var dangerousEnemies = enemyEntities
                    .Where(x => x.EntityType != EntityType.BuilderUnit)
                    .Where(x => playerView.EntityProperties[x.EntityType].Attack.HasValue && entity.Position.Dist(x.Position, playerView.EntityProperties[x.EntityType].Size) <= playerView.EntityProperties[x.EntityType].Attack.Value.AttackRange + (x.EntityType == EntityType.Turret ? 1 : 2))
                    .OrderBy(x => entity.Position.Dist(x.Position, playerView.EntityProperties[x.EntityType].Size)).ToArray();
                entitiesCanPotentiallyRepair = GetEntitiesCanRepair(entity, Info.MyEntitiesByType[EntityType.RangedBase].Where(x => x.Active).Any() ? 5 : 7).Where(x => x.EntityType.In(Info.EntityTypesForFastRepair) || x.Dist(entity) == 1).ToArray();
                var bestRepairerPosition = Info.VecInf;
                if (entitiesCanPotentiallyRepair.Length > 0)
                {
                    bestRepairerPosition = GetBestRepairerPosition(entity, entitiesCanPotentiallyRepair[0].EntityType, entitiesCanPotentiallyRepair[0].Position);
                }
                if (dangerousEnemies.Length > 0)
                {
                    var direction = entity.Position.Minus(dangerousEnemies[0].Position);
                    result.Target = entity.Position.Plus(direction);
                    entityAction.AttackAction = null;
                }
                else if (!bestRepairerPosition.Eq(Info.VecInf))
                {
                    result.Target = bestRepairerPosition;
                    entityAction.AttackAction = null;
                }
                else if (Info.AvailableResources.Count > 0)
                {
                    result.Target = Info.AvailableResources.OrderBy(x => x.Dist(entity.Position)).First();
                }
                else if (resourceEntities.Length > 0)
                {
                    result.Target = resourceEntities.OrderBy(x => x.Dist(entity.Position)).First().Position;
                }
                else
                {
                    result.Target = Info.VecInf;
                }
            }
            result.Target = new Vec2Int(System.Math.Clamp(result.Target.X, 0, playerView.MapSize - 1), System.Math.Clamp(result.Target.Y, 0, playerView.MapSize - 1));
            return result;
        }

        public Action GetAction(PlayerView playerView, DebugInterface debugInterface)
        {
            Info.timer[9].Start();
            Info.timer[0].Start();
            Init(playerView, debugInterface);
            Info.timer[0].Stop();
            Info.EntityActionData = new Dictionary<int, EntityAction>();
            if (Info.MyEntities.Length == 0)
            {
                return new Action(Info.EntityActionData);
            }
            BuildBase(EntityType.House);
            BuildBase(EntityType.BuilderBase);
            BuildBase(EntityType.RangedBase);
            BuildBase(EntityType.Turret);
            CancelConstructionInterferingWithRangedBase();
            foreach (var entity in Info.MyEntities)
            {
                var entityProperties = playerView.EntityProperties[entity.EntityType];
                var entityAction = new EntityAction();
                if (Info.myActionsById.ContainsKey(entity.Id))
                {
                    if (playerView.CurrentTick > 10 && entity.Id.In(builderIdByType.Values.ToArray()))
                    {
                        if (myEntitiesByIdHistory[playerView.CurrentTick - 3].ContainsKey(entity.Id))
                        {
                            if (entity.Position.Eq(myEntitiesByIdHistory[playerView.CurrentTick - 2][entity.Id].Position) && !entity.Position.Eq(myEntitiesByIdHistory[playerView.CurrentTick - 1][entity.Id].Position) && (myEntitiesByIdHistory[playerView.CurrentTick - 1][entity.Id].Position.Eq(myEntitiesByIdHistory[playerView.CurrentTick - 3][entity.Id].Position)))
                            {
                                Info.EntityActionData.Add(entity.Id, new EntityAction(null, null, null, null));
                                continue;
                            }
                        }
                    }
                    Info.EntityActionData.Add(entity.Id, Info.myActionsById[entity.Id]);
                    if (!entity.Id.In(builderIdByType.Values.ToArray()) && !entity.Id.In(repairerIdByType.Values.ToArray()))
                    {
                        if (GetRepairAction(entity, ref entityAction).Target > 0)
                        {
                            Info.EntityActionData.Remove(entity.Id);
                            Info.myActionsById.Remove(entity.Id);
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                if (entityProperties.Attack.HasValue)
                {
                    entityAction.AttackAction = GetAttackAction(entity);
                }
                if (entityProperties.Build.HasValue)
                {
                    entityAction.BuildAction = GetBuildAction(entity);
                }
                if (entity.EntityType == EntityType.BuilderUnit)
                {
                    entityAction.RepairAction = GetRepairAction(entity, ref entityAction);
                }
                if (entityProperties.CanMove)
                {
                    Info.timer[1].Start();
                    entityAction.MoveAction = GetMoveAction(entity, ref entityAction);
                    Info.timer[1].Stop();
                }
                Info.EntityActionData.Add(entity.Id, entityAction);
            }
            if (Info.ForestToCut.Count > 0)
            {
                CutForest();
            }
            Final();
            lastAction = new Action(Info.EntityActionData);
            Info.timer[9].Stop();
            return lastAction;
        }

        public void CutForest()
        {
            foreach (var entity in Info.MyEntitiesByType[EntityType.RangedUnit].OrderBy(x => x.Position.Dist(Info.VecZero)))
            {
                if (Info.EntityActionData[entity.Id].AttackAction.HasValue && Info.EntityActionData[entity.Id].AttackAction.Value.Target != null)
                {
                    continue;
                }
                if (Info.ProjectedMap.GetCell(Info.EntityActionData[entity.Id].MoveAction.Value.Target).Entity.PlayerId != Info.PlayerView.MyId)
                {
                    continue;
                }
                var canHit = Info.ForestToCut.Where(x => x.Dist(entity) <= Info.PlayerView.EntityProperties[entity.EntityType].Attack.Value.AttackRange);
                if (canHit.Any())
                {
                    var targetEntity = canHit.OrderBy(x => x.Health).First();
                    var attackAction = new AttackAction(targetEntity.Id, null);
                    Info.EntityActionData[entity.Id] = new EntityAction(Info.EntityActionData[entity.Id].MoveAction, null, attackAction, null);
                    Info.ForestToCut.Remove(targetEntity);
                    targetEntity.Health -= Info.PlayerView.EntityProperties[entity.EntityType].Attack.Value.Damage;
                    if (targetEntity.Health > 0)
                    {
                        Info.ForestToCut.Add(targetEntity);
                    }
                }
            }
        }

        public void DebugUpdate(PlayerView playerView, DebugInterface debugInterface) 
        {
            if (playerView.CurrentTick == 0 || Info.MyEntities.Length == 0)
            {
                return;
            }
            debugInterface.Send(new DebugCommand.SetAutoFlush(false));
            debugInterface.Send(new DebugCommand.Clear());

            //DrawMoves();
            //DrawReacheabilityMap();
            //DrawAvailableResources();
            //DrawNowVisible();
            //DrawVisibleEdge();
            //DrawResources();
            //DrawProjectedMap();
            //DrawPathMap();
            //DrawTurretMap();
            //DrawBuilderDangerMap();
            //DrawWings();

            debugInterface.GetState();
            debugInterface.Send(new DebugCommand.Flush());
        }

        private void DrawWings()
        {
            var vertices = new List<ColoredVertex>();
            foreach (var entityId in Info.LeftWing)
            {
                var entity = myEntitiesById[entityId];
                var v1 = new Vec2Float(entity.Position.X, entity.Position.Y);
                var v2 = new Vec2Float(entity.Position.X + 1, entity.Position.Y);
                var v3 = new Vec2Float(entity.Position.X + 0.5f, entity.Position.Y + 1);
                vertices.Add(new ColoredVertex(v1, new Vec2Float(0, 0), new Color(1, 1, 0, 0.6f)));
                vertices.Add(new ColoredVertex(v2, new Vec2Float(0, 0), new Color(1, 1, 0, 0.6f)));
                vertices.Add(new ColoredVertex(v3, new Vec2Float(0, 0), new Color(1, 1, 0, 0.6f)));
            }
            foreach (var entityId in Info.RightWing)
            {
                var entity = myEntitiesById[entityId];
                var v1 = new Vec2Float(entity.Position.X, entity.Position.Y);
                var v2 = new Vec2Float(entity.Position.X + 1, entity.Position.Y);
                var v3 = new Vec2Float(entity.Position.X + 0.5f, entity.Position.Y + 1);
                vertices.Add(new ColoredVertex(v1, new Vec2Float(0, 0), new Color(1, 1, 0, 0.6f)));
                vertices.Add(new ColoredVertex(v2, new Vec2Float(0, 0), new Color(1, 1, 0, 0.6f)));
                vertices.Add(new ColoredVertex(v3, new Vec2Float(0, 0), new Color(1, 1, 0, 0.6f)));
            }
            var triangles = new DebugData.Primitives(vertices.ToArray(), PrimitiveType.Triangles);
            Info.DebugInterface.Send(new DebugCommand.Add(triangles));
        }

        private void DrawMoves()
        {
            var vertices = new List<ColoredVertex>();
            foreach (var action in lastAction.EntityActions)
            {
                var entity = myEntitiesById[action.Key];
                var moveT = action.Value.MoveAction;
                if (entity.EntityType == EntityType.BuilderUnit && moveT.HasValue)
                {
                    var move = moveT.Value;
                    var from = new Vec2Float(entity.Position.X + 0.5f, entity.Position.Y + 0.5f);
                    var to = new Vec2Float(move.Target.X + 0.5f, move.Target.Y + 0.5f);
                    vertices.Add(new ColoredVertex(from, new Vec2Float(0, 0), new Color(1, 0, 0, 1)));
                    vertices.Add(new ColoredVertex(to, new Vec2Float(0, 0), new Color(0, 1, 0, 1)));
                }
            }
            var lines = new DebugData.Primitives(vertices.ToArray(), PrimitiveType.Lines);
            Info.DebugInterface.Send(new DebugCommand.Add(lines));
        }

        private void DrawReacheabilityMap()
        {
            if (playerView.CurrentTick < reacheabilityTick)
            {
                for (int i = 0; i < Info.PlayerView.MapSize; i++)
                {
                    for (int j = 0; j < Info.PlayerView.MapSize; j++)
                    {
                        var text = new DebugData.PlacedText(new ColoredVertex(new Vec2Float(i, j), new Vec2Float(), new Color(1, 0, 0, 0.5f)), reacheabilityMap[i, j].ToString(), 0f, 14);
                        Info.DebugInterface.Send(new DebugCommand.Add(text));
                    }
                }
            }
        }

        private void DrawPathMap()
        {
            if (MMM != null)
            {
                for (int i = 0; i < Info.PlayerView.MapSize; i++)
                {
                    for (int j = 0; j < Info.PlayerView.MapSize; j++)
                    {
                        if (MMM[i, j].G < int.MaxValue)
                        {
                            var text = new DebugData.PlacedText(new ColoredVertex(new Vec2Float(i, j), new Vec2Float(), new Color(1, 0, 0, 0.5f)), MMM[i, j].G.ToString(), 0f, 14);
                            Info.DebugInterface.Send(new DebugCommand.Add(text));
                        }
                    }
                }
            }
        }

        private void DrawTurretMap()
        {
            if (playerView.CurrentTick > 250)
            {
                for (int i = 0; i < Info.PlayerView.MapSize; i++)
                {
                    for (int j = 0; j < Info.PlayerView.MapSize; j++)
                    {
                        if (Info.ProjectedMap.dangerMap[i, j] != 0)
                        {
                            var text = new DebugData.PlacedText(new ColoredVertex(new Vec2Float(i, j), new Vec2Float(), new Color(1, 0, 0, 0.5f)), Info.ProjectedMap.dangerMap[i, j].ToString(), 0f, 14);
                            Info.DebugInterface.Send(new DebugCommand.Add(text));
                        }
                        if (Info.ProjectedMap.preyMap[i, j] != 0)
                        {
                            var text = new DebugData.PlacedText(new ColoredVertex(new Vec2Float(i, j), new Vec2Float(), new Color(0, 0, 1, 0.7f)), Info.ProjectedMap.preyMap[i, j].ToString(), 0f, 14);
                            Info.DebugInterface.Send(new DebugCommand.Add(text));
                        }
                    }
                }
            }
        }

        private void DrawBuilderDangerMap()
        {
            for (int i = 0; i<Info.PlayerView.MapSize; i++)
            {
                for (int j = 0; j<Info.PlayerView.MapSize; j++)
                {
                    if (Info.ProjectedMap.builderDangerMap[i, j] != 0)
                    {
                        var text = new DebugData.PlacedText(new ColoredVertex(new Vec2Float(i, j), new Vec2Float(), new Color(1, 0, 0, 0.5f)), Info.ProjectedMap.builderDangerMap[i, j].ToString(), 0f, 14);
                        Info.DebugInterface.Send(new DebugCommand.Add(text));
                    }
                }
            }
        }

        private void DrawAvailableResources()
        {
            var vertices = new List<ColoredVertex>();
            foreach (var res in Info.AvailableResources)
            {
                var v1 = new Vec2Float(res.X, res.Y);
                var v2 = new Vec2Float(res.X + 1, res.Y);
                var v3 = new Vec2Float(res.X + 0.5f, res.Y + 1);
                vertices.Add(new ColoredVertex(v1, new Vec2Float(0, 0), new Color(1, 1, 0, 0.6f)));
                vertices.Add(new ColoredVertex(v2, new Vec2Float(0, 0), new Color(1, 1, 0, 0.6f)));
                vertices.Add(new ColoredVertex(v3, new Vec2Float(0, 0), new Color(1, 1, 0, 0.6f)));
            }
            var triangles = new DebugData.Primitives(vertices.ToArray(), PrimitiveType.Triangles);
            Info.DebugInterface.Send(new DebugCommand.Add(triangles));
        }

        private void DrawNowVisible()
        {
            var vertices = new List<ColoredVertex>();

            for (int i = 0; i < Info.PlayerView.MapSize; i++)
            {
                for (int j = 0; j < Info.PlayerView.MapSize; j++)
                {
                    if (Info.NowVisible.map[i, j])
                    {
                        var pos = new Vec2Int(i, j);
                        var v1 = new Vec2Float(pos.X, pos.Y);
                        var v2 = new Vec2Float(pos.X + 1, pos.Y);
                        var v3 = new Vec2Float(pos.X + 0.5f, pos.Y + 1);
                        var color = new Color(Info.OnceVisible.map[i, j].Entity.Id > 0 ? 1 : 0, 1, 0, 0.5f);
                        vertices.Add(new ColoredVertex(v1, new Vec2Float(0, 0), color));
                        vertices.Add(new ColoredVertex(v2, new Vec2Float(0, 0), color));
                        vertices.Add(new ColoredVertex(v3, new Vec2Float(0, 0), color));
                    }
                }
            }
            var triangles = new DebugData.Primitives(vertices.ToArray(), PrimitiveType.Triangles);
            Info.DebugInterface.Send(new DebugCommand.Add(triangles));
        }

        private void DrawVisibleEdge()
        {
            var vertices = new List<ColoredVertex>();

            for (int i = 0; i < Info.PlayerView.MapSize; i++)
            {
                for (int j = 0; j < Info.PlayerView.MapSize; j++)
                {
                    if (Info.OnceVisible.map[i, j].Visible)
                    {
                        var pos = new Vec2Int(i, j);
                        var v1 = new Vec2Float(pos.X, pos.Y);
                        var v2 = new Vec2Float(pos.X + 1, pos.Y);
                        var v3 = new Vec2Float(pos.X + 0.5f, pos.Y + 1);
                        var color = new Color(Info.OnceVisible.map[i, j].Entity.Id > 0 ? 1 : 0, 1, 0, 0.5f);
                        vertices.Add(new ColoredVertex(v1, new Vec2Float(0, 0), color));
                        vertices.Add(new ColoredVertex(v2, new Vec2Float(0, 0), color));
                        vertices.Add(new ColoredVertex(v3, new Vec2Float(0, 0), color));
                    }
                }
            }

            foreach (var pos in Info.OnceVisible.edge)
            {
                var v1 = new Vec2Float(pos.X, pos.Y);
                var v2 = new Vec2Float(pos.X + 1, pos.Y);
                var v3 = new Vec2Float(pos.X + 0.5f, pos.Y + 1);
                vertices.Add(new ColoredVertex(v1, new Vec2Float(0, 0), new Color(0, 0, 1, 0.5f)));
                vertices.Add(new ColoredVertex(v2, new Vec2Float(0, 0), new Color(0, 0, 1, 0.5f)));
                vertices.Add(new ColoredVertex(v3, new Vec2Float(0, 0), new Color(0, 0, 1, 0.5f)));
            }
            
            var triangles = new DebugData.Primitives(vertices.ToArray(), PrimitiveType.Triangles);
            Info.DebugInterface.Send(new DebugCommand.Add(triangles));
        }

        private void DrawResources()
        {
            var vertices = new List<ColoredVertex>();
            for (int i = 0; i < Info.PlayerView.MapSize; i++)
            {
                for (int j = 0; j < Info.PlayerView.MapSize; j++)
                {
                    if (Info.ResourceMap.map[i, j].Entity.Id > 0)
                    {
                        var pos = new Vec2Int(i, j);
                        var v1 = new Vec2Float(pos.X, pos.Y);
                        var v2 = new Vec2Float(pos.X + 1, pos.Y);
                        var v3 = new Vec2Float(pos.X + 0.5f, pos.Y + 1);
                        var color = new Color(Info.OnceVisible.map[i, j].Entity.Id > 0 ? 1 : 0, 1, 0, 0.5f);
                        vertices.Add(new ColoredVertex(v1, new Vec2Float(0, 0), color));
                        vertices.Add(new ColoredVertex(v2, new Vec2Float(0, 0), color));
                        vertices.Add(new ColoredVertex(v3, new Vec2Float(0, 0), color));
                    }
                }
            }
            var triangles = new DebugData.Primitives(vertices.ToArray(), PrimitiveType.Triangles);
            Info.DebugInterface.Send(new DebugCommand.Add(triangles));
        }

        private void DrawProjectedMap()
        {
            var vertices = new List<ColoredVertex>();
            Vec2Float v1;
            Vec2Float v2;
            Vec2Float v3;
            Color color;
            for (int i = 0; i < Info.PlayerView.MapSize; i++)
            {
                for (int j = 0; j < Info.PlayerView.MapSize; j++)
                {
                    var pos = new Vec2Int(i, j);
                    if (Info.ProjectedMap.map[i, j].Entity.EntityType == EntityType.Resource)
                    {
                        v1 = new Vec2Float(pos.X, pos.Y);
                        v2 = new Vec2Float(pos.X + 1, pos.Y);
                        v3 = new Vec2Float(pos.X + 0.5f, pos.Y + 1);
                        color = new Color(1, 0, 0, 0.5f);
                    } else if (Info.ProjectedMap.map[i, j].Entity.Id == 0)
                    {
                        v1 = new Vec2Float(pos.X, pos.Y);
                        v2 = new Vec2Float(pos.X + 1, pos.Y);
                        v3 = new Vec2Float(pos.X + 0.5f, pos.Y + 0.5f);
                        color = new Color(0.5f, 0, 0, 0.5f);
                    } else
                    {
                        v1 = new Vec2Float(pos.X, pos.Y + 1);
                        v2 = new Vec2Float(pos.X + 1, pos.Y + 1);
                        v3 = new Vec2Float(pos.X + 0.5f, pos.Y);
                        color = new Color(0, 1, 0, 0.5f);
                    }
                    vertices.Add(new ColoredVertex(v1, new Vec2Float(0, 0), color));
                    vertices.Add(new ColoredVertex(v2, new Vec2Float(0, 0), color));
                    vertices.Add(new ColoredVertex(v3, new Vec2Float(0, 0), color));
                }
            }
            var triangles = new DebugData.Primitives(vertices.ToArray(), PrimitiveType.Triangles);
            Info.DebugInterface.Send(new DebugCommand.Add(triangles));
        }
    }

    public static class Ext
    {
        public static T[] Shuffle<T>(this T[] arr)
        {
            var result = arr.Clone() as T[];
            for (int i = 0; i < arr.Length * 2; i++)
            {
                var q1 = Info.Rnd.Next(arr.Length);
                var q2 = Info.Rnd.Next(arr.Length);
                var o = result[q1];
                result[q1] = result[q2];
                result[q2] = o;
            }
            return result;
        }

        public static bool In<T>(this T val, params T[] values) where T : struct
        {
            return values.Contains(val);
        }

        public static bool Eq(this double q, double w)
        {
            return System.Math.Abs(q - w) < Info.Eps;
        }

        public static bool More(this double q, double w)
        {
            return q - w > Info.Eps;
        }

        public static bool MoreEq(this double q, double w)
        {
            return q.More(w) || q.Eq(w);
        }

        public static bool Less(this double q, double w)
        {
            return w.More(q);
        }

        public static bool LessEq(this double q, double w)
        {
            return w.MoreEq(q);
        }

        public static bool Between(this int q, int w, int e)
        {
            return q > w && q < e;
        }

        public static bool BetweenEq(this int q, int w, int e)
        {
            return q >= w && q <= e;
        }

        public static bool Between(this Vec2Int q, Vec2Int w, Vec2Int e)
        {
            return q.X.Between(w.X, e.X) && q.Y.Between(w.Y, e.Y);
        }

        public static bool BetweenEq(this Vec2Int q, Vec2Int w, Vec2Int e)
        {
            return q.X.BetweenEq(w.X, e.X) && q.Y.BetweenEq(w.Y, e.Y);
        }

        public static bool Eq(this Vec2Int q, Vec2Int w)
        {
            return q.X == w.X && q.Y == w.Y;
        }

        public static Vec2Int Plus(this Vec2Int q, Vec2Int w)
        {
            return new Vec2Int(q.X + w.X, q.Y + w.Y);
        }

        public static Vec2Int Minus(this Vec2Int q, Vec2Int w)
        {
            return new Vec2Int(q.X - w.X, q.Y - w.Y);
        }

        public static Vec2Int Mult(this Vec2Int q, int w)
        {
            return new Vec2Int(q.X * w, q.Y * w);
        }

        public static int Dist(this Vec2Int q, Vec2Int w, int entitySize = 1)
        {
            if (entitySize == 1)
            {
                return System.Math.Abs(q.X - w.X) + System.Math.Abs(q.Y - w.Y);
            }
            if (q.X.BetweenEq(w.X, w.X + entitySize - 1) && q.Y.BetweenEq(w.Y, w.Y + entitySize - 1))
            {
                return 0;
            }
            if (q.X.BetweenEq(w.X, w.X + entitySize - 1))
            {
                return System.Math.Min(System.Math.Abs(q.Y - w.Y), System.Math.Abs(q.Y - (w.Y + entitySize - 1)));
            }
            if (q.Y.BetweenEq(w.Y, w.Y + entitySize - 1))
            {
                return System.Math.Min(System.Math.Abs(q.X - w.X), System.Math.Abs(q.X - (w.X + entitySize - 1)));
            }
            var result = int.MaxValue;
            for (int i = w.X; i < w.X + entitySize; i += entitySize - 1)
            {
                for (int j = w.Y; j < w.Y + entitySize; j += entitySize - 1)
                {
                    result = System.Math.Min(result, q.Dist(new Vec2Int(i, j)));
                }
            }
            return result;
        }

        public static int Dist(this Vec2Int q, Entity w)
        {
            return q.Dist(w.Position, Info.PlayerView.EntityProperties[w.EntityType].Size);
        }

        public static int Dist(this Entity q, Entity w)
        {

            if (Info.PlayerView.EntityProperties[q.EntityType].Size > Info.PlayerView.EntityProperties[w.EntityType].Size)
            {
                var o = q;
                q = w;
                w = o;
            }
            if (Info.PlayerView.EntityProperties[q.EntityType].Size == 1)
            {
                return q.Position.Dist(w.Position, Info.PlayerView.EntityProperties[w.EntityType].Size);
            }
            var corners = new Vec2Int[] { new Vec2Int(0, 0), new Vec2Int(1, 0), new Vec2Int(0, 1), new Vec2Int(1, 1) };
            var result = int.MaxValue;
            foreach (var d in corners)
            {
                var pos = q.Position.Plus(d.Mult(Info.PlayerView.EntityProperties[q.EntityType].Size));
                result = System.Math.Min(result, pos.Dist(w.Position, Info.PlayerView.EntityProperties[w.EntityType].Size));
            }
            return result;
        }

        public static int Dist(this Entity q, Vec2Int w, int entitySize = 1)
        {
            var qPos = q.Position;
            var qSize = Info.PlayerView.EntityProperties[q.EntityType].Size;
            if (qSize > entitySize)
            {
                var o = qPos;
                qPos = w;
                w = o;
                var o1 = qSize;
                qSize = entitySize;
                entitySize = o1;
            }
            if (qSize == 1)
            {
                return qPos.Dist(w, entitySize);
            }
            var corners = new Vec2Int[] { new Vec2Int(0, 0), new Vec2Int(1, 0), new Vec2Int(0, 1), new Vec2Int(1, 1) };
            var result = int.MaxValue;
            foreach (var d in corners)
            {
                var pos = qPos.Plus(d.Mult(qSize));
                result = System.Math.Min(result, pos.Dist(w, entitySize));
            }
            return result;
        }
    }

    public static class Info
    {
        public static double Eps = 1e-5;
        public static System.Random Rnd = new System.Random(1234567890);
        public static DebugInterface DebugInterface;
        public static Stopwatch[] timer = new Stopwatch[10] { new Stopwatch(), new Stopwatch(), new Stopwatch(), new Stopwatch(), new Stopwatch(), new Stopwatch(), new Stopwatch(), new Stopwatch(), new Stopwatch(), new Stopwatch() };
        public static bool TL;
        public static PlayerView PlayerView { get; set; }
        public static int Round { get; set; }
        public static readonly Vec2Int VecZero = new Vec2Int(0, 0);
        public static Vec2Int VecInf;
        public static readonly Vec2Int[] Vec4 = new Vec2Int[4] { new Vec2Int(0, 1), new Vec2Int(1, 0), new Vec2Int(-1, 0), new Vec2Int(0, -1) };
        public static readonly Vec2Int[] Vec4x = new Vec2Int[4] { new Vec2Int(-1, 1), new Vec2Int(1, 1), new Vec2Int(1, -1), new Vec2Int(-1, -1) };
        public static Vec2Int[] Vec8;
        public static int RepairThreshold;
        public static EntityType[] EntityTypesForFastRepair = new EntityType[4] { EntityType.BuilderBase, EntityType.RangedBase, EntityType.MeleeBase, EntityType.Turret };
        public static int precountedCloseEnemiesRange;
        public static int precountedCloseAlliesRange;
        public static Dictionary<int, Entity[]> closeAllyEntities;
        public static HashSet<int> EntitiesIds;
        public static Dictionary<int, Entity> EntitiesById;
        public static Entity[] MyEntities;
        public static HashSet<int> MyEntitiesIds;
        public static Dictionary<EntityType, Entity[]> MyEntitiesByType;
        public static Dictionary<int, Entity[]> CloseEnemyEntities;
        public static HashSet<Vec2Int> AvailableResources;
        public static Dictionary<int, int> BuildingsRepairersCount;
        public static NowVisible NowVisible;
        public static OnceVisible OnceVisible;
        public static ResourceMap ResourceMap;
        public static ProjectedMap ProjectedMap;
        public static PathMap PathMap;
        public static Dictionary<int, EntityAction> myActionsById = new Dictionary<int, EntityAction>();
        public static HashSet<Entity> ForestToCut = new HashSet<Entity>();
        public static Dictionary<int, EntityAction> EntityActionData = new Dictionary<int, EntityAction>();
        public static HashSet<int> LeftWing = new HashSet<int>();
        public static HashSet<int> RightWing = new HashSet<int>();
        public static Dictionary<int, Vec2Int> WingDirection = new Dictionary<int, Vec2Int>();
        private static int id = 10000000;

        public static bool OutOfBounds(Vec2Int pos)
        {
            return pos.X < 0 || pos.X >= PlayerView.MapSize || pos.Y < 0 || pos.Y >= PlayerView.MapSize;
        }

        public static bool Visible(Vec2Int pos)
        {
            return NowVisible.map[pos.X, pos.Y];
        }

        public static bool Visible(Entity entity)
        {
            for (int i = 0; i < PlayerView.EntityProperties[entity.EntityType].Size; i++)
            {
                for (int j = 0; j < PlayerView.EntityProperties[entity.EntityType].Size; j++)
                {
                    if (NowVisible.map[entity.Position.X + i, entity.Position.Y + j])
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool MyTerritory(Vec2Int pos)
        {
            if (Round == 3)
            {
                return pos.X + pos.Y < PlayerView.MapSize;
            }
            return pos.X < PlayerView.MapSize / 2 && pos.Y < PlayerView.MapSize / 2;
        }

        public static Vec2Int[] GetMirroredPoints(Vec2Int pos)
        {
            var opposite = new Vec2Int(PlayerView.MapSize - 1 - pos.X, PlayerView.MapSize - 1 - pos.Y);
            if (Round == 3)
            {
                return new Vec2Int[] { opposite };
            }
            return new Vec2Int[] { new Vec2Int(pos.Y, PlayerView.MapSize - 1 - pos.X), new Vec2Int(PlayerView.MapSize - 1 - pos.Y, pos.X), opposite };
        }

        public static int GetId()
        {
            id++;
            return id;
        }

        public static bool Gathering(Entity entity)
        {
            if (entity.EntityType != EntityType.BuilderUnit)
            {
                return false;
            }
            foreach (var d in Info.Vec4)
            {
                var pos = entity.Position.Plus(d);
                if (!OutOfBounds(pos) && ProjectedMap.map[pos.X, pos.Y].Entity.EntityType == EntityType.Resource)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool Repairing(Entity entity)
        {
            if (entity.EntityType != EntityType.BuilderUnit)
            {
                return false;
            }
            foreach (var d in Info.Vec4)
            {
                var pos = entity.Position.Plus(d);
                if (!OutOfBounds(pos) && ProjectedMap.map[pos.X, pos.Y].Entity.PlayerId == PlayerView.MyId && (!ProjectedMap.map[pos.X, pos.Y].Entity.Active || ProjectedMap.map[pos.X, pos.Y].Entity.Health < PlayerView.EntityProperties[ProjectedMap.map[pos.X, pos.Y].Entity.EntityType].MaxHealth - Info.RepairThreshold) && PlayerView.EntityProperties[entity.EntityType].Repair.Value.ValidTargets.Contains(ProjectedMap.map[pos.X, pos.Y].Entity.EntityType))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool Occupied(Entity entity)
        {
            return myActionsById.Keys.Contains(entity.Id);
        }

        public static bool MyRangedWillNotMove(Entity entity)
        {
            return Info.CloseEnemyEntities.ContainsKey(entity.Id) && Info.CloseEnemyEntities[entity.Id].Length > 0;
        }

        public static List<Vec2Int> GetPointsInRange(Entity entity, int range)
        {
            var result = new List<Vec2Int>();
            var size = Info.PlayerView.EntityProperties[entity.EntityType].Size;
            var x = new Vec2Int(System.Math.Max(0, entity.Position.X - range), System.Math.Min(Info.PlayerView.MapSize - 1, entity.Position.X + size - 1 + range));
            var y = new Vec2Int(System.Math.Max(0, entity.Position.Y - range), System.Math.Min(Info.PlayerView.MapSize - 1, entity.Position.Y + size - 1 + range));
            for (int i = x.X; i <= x.Y; i++)
            {
                for (int j = y.X; j <= y.Y; j++)
                {
                    var pos = new Vec2Int(i, j);
                    var dist = entity.Dist(pos);
                    if (dist <= range)
                    {
                        result.Add(pos);
                    }
                    else
                    {
                        j += dist - range - 1;
                    }
                }
            }
            return result;
        }
    }

    public struct MapCell
    {
        public Entity Entity { get; set; }
        public bool Visible { get; set; }

        public MapCell(Entity entity, bool visible = false)
        {
            Entity = entity;
            Visible = visible;
        }
    }

    public class PathMapCellComparer : IComparer<PathMapCell>
    {
        private bool bfs, builder;

        public PathMapCellComparer(bool bfs = false, bool builder = false)
        {
            this.bfs = bfs;
            this.builder = builder;
        }

        private int GetIndex(PathMapCell q)
        {
            if (q.Coord.Y > q.Coord.X)
            {
                return (Info.PlayerView.MapSize - q.Coord.Y) * Info.PlayerView.MapSize + q.Coord.X;
            }
            else
            {
                return Info.PlayerView.MapSize * Info.PlayerView.MapSize + (Info.PlayerView.MapSize - q.Coord.X) * Info.PlayerView.MapSize + q.Coord.Y;
            }
        }

        public int Compare(PathMapCell q, PathMapCell w)
        {
            if (bfs)
            {
                if (!q.G.Eq(w.G))
                {
                    return (int)((q.G - w.G) * 300);
                }
            }
            else if (!q.F.Eq(w.F))
            {
                return (int)((q.F - w.F) * 300);
            }

            if (builder)
            {
                return GetIndex(q) - GetIndex(w);
            }

            var d = (q.Coord.X + q.Coord.Y) - (w.Coord.X + w.Coord.Y);
            if (d == 0)
            {
                d = q.Coord.X - w.Coord.X;
            }
            return -d;
        }
    }

    public struct PathMapCell
    {
        public Vec2Int Coord { get; set; }
        public Vec2Int Parent { get; set; }
        public double F { get; set; }
        public double G { get; set; }

        public PathMapCell(Vec2Int coord, Vec2Int parent, int f = int.MaxValue, int g = int.MaxValue)
        {
            Coord = coord;
            Parent = parent;
            F = f;
            G = g;
        }
    }

    public abstract class Map<T>
    {
        public T[,] map;

        public T GetCell(Vec2Int pos)
        {
            return map[pos.X, pos.Y];
        }
    }

    public class PathMap
    {
        public PathMapCell[,] map;
        public List<Vec2Int> changedCells;
        public SortedSet<PathMapCell> queue;

        public PathMap()
        {
            map = new PathMapCell[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            for (int i = 0; i < Info.PlayerView.MapSize; i++)
            {
                for (int j = 0; j < Info.PlayerView.MapSize; j++)
                {
                    map[i, j] = new PathMapCell(new Vec2Int(i, j), new Vec2Int(i, j));
                }
            }
            changedCells = new List<Vec2Int>();
            queue = new SortedSet<PathMapCell>(new PathMapCellComparer());
        }

        public int GetCost(Entity entity, Vec2Int pos)
        {
            var cellEntity = Info.ProjectedMap.GetCell(pos).Entity;
            if (entity.EntityType == EntityType.BuilderUnit)
            {
                if (Info.ProjectedMap.builderDangerMap[pos.X, pos.Y] > 0)
                {
                    if (Info.ProjectedMap.builderDangerMap[entity.Position.X, entity.Position.Y] <= 0)
                    {
                        return int.MaxValue;
                    }
                    else
                    {
                        return 2;
                    }
                }
                if (cellEntity.Id == 0)
                {
                    return 1;
                }
                if (cellEntity.EntityType == EntityType.Resource)
                {
                    var damage = Info.PlayerView.EntityProperties[entity.EntityType].Attack.Value.Damage;
                    return System.Math.Min(4, (cellEntity.Health + damage - 1) / damage + 1);
                }
                if (cellEntity.EntityType == EntityType.Wall)
                {
                    var damage = Info.PlayerView.EntityProperties[entity.EntityType].Attack.Value.Damage;
                    return (cellEntity.Health + damage - 1) / damage + 1;
                }
                if (cellEntity.Id != 0 && !Info.PlayerView.EntityProperties[cellEntity.EntityType].CanMove)
                {
                    if (Info.ProjectedMap.builderDangerMap[entity.Position.X, entity.Position.Y] <= 0 && cellEntity.PlayerId == Info.PlayerView.MyId && cellEntity.Health < Info.PlayerView.EntityProperties[cellEntity.EntityType].MaxHealth - Info.RepairThreshold && (cellEntity.EntityType.In(Info.EntityTypesForFastRepair) || !cellEntity.Active))
                    {
                        return 1;
                    }
                    if (cellEntity.PlayerId != Info.PlayerView.MyId)
                    {
                        return 3;
                    }
                    return int.MaxValue;
                }
                if (cellEntity.EntityType == EntityType.RangedUnit)
                {
                    if (Info.MyRangedWillNotMove(cellEntity))
                    {
                        return int.MaxValue;
                    }
                    return 3;
                }
                if (cellEntity.EntityType != EntityType.BuilderUnit)
                {
                    return 2;
                }
                if (Info.Repairing(cellEntity) || Info.Gathering(cellEntity))
                {
                    return int.MaxValue;
                }
                return 1;
            }
            if (Info.ProjectedMap.dangerMap[pos.X, pos.Y] > 0 || Info.ProjectedMap.preyMap[pos.X, pos.Y] > 0)
            {
                return int.MaxValue;
            }
            if (cellEntity.Id == 0)
            {
                return 1;
            }
            if (cellEntity.EntityType == EntityType.Resource || cellEntity.EntityType == EntityType.Wall)
            {
                if (Info.LeftWing.Contains(entity.Id) || Info.RightWing.Contains(entity.Id))
                {
                    return 2;
                }
                var damage = Info.PlayerView.EntityProperties[entity.EntityType].Attack.Value.Damage;
                if (entity.EntityType == EntityType.RangedUnit)
                {
                    damage *= System.Math.Min(5, Info.closeAllyEntities[entity.Id].Length / 2 + 1);
                }
                return (cellEntity.Health + damage - 1) / damage + 1;
            }
            if (cellEntity.Id != 0 && !Info.PlayerView.EntityProperties[cellEntity.EntityType].CanMove)
            {
                return int.MaxValue;
            }
            if (cellEntity.Id != 0 && (Info.ProjectedMap.dangerMap[pos.X, pos.Y] < 0 || Info.ProjectedMap.preyMap[pos.X, pos.Y] < 0))
            {
                return int.MaxValue;
            }
            if (cellEntity.EntityType == EntityType.RangedUnit)
            {
                if (Info.MyRangedWillNotMove(cellEntity))
                {
                    return int.MaxValue;
                }
                return 2;
            }
            if (cellEntity.EntityType != EntityType.BuilderUnit)
            {
                return 1;
            }
            if (Info.Repairing(cellEntity) || Info.Gathering(cellEntity))
            {
                return int.MaxValue;
            }
            return 1;
        }

        public Vec2Int GetFirstStep(Vec2Int last)
        {
            var result = last;
            while (map[last.X, last.Y].F.More(0))
            {
                result = last;
                last = map[last.X, last.Y].Parent;
            }
            return result;
        }

        public Vec2Int GetPath(Entity entity, Vec2Int to, int repairDist = 0)
        {
            Info.timer[6].Start();
            changedCells = new List<Vec2Int>();
            queue = new SortedSet<PathMapCell>(new PathMapCellComparer(bfs: Info.OutOfBounds(to), builder: repairDist > 0));
            var from = entity.Position;
            map[from.X, from.Y] = new PathMapCell(from, from, 0, 0);
            changedCells.Add(from);
            queue.Add(map[from.X, from.Y]);
            var closest = from;
            double closestDist = from.Dist(to);
            while (queue.Count > 0)
            {
                var current = queue.Min;
                queue.Remove(current);
                if (current.Coord.Eq(to))
                {
                    closest = current.Coord;
                    break;
                }
                if (entity.EntityType == EntityType.BuilderUnit && closestDist.Eq(0) && current.G > repairDist)
                {
                    break;
                }
                if (entity.EntityType == EntityType.RangedUnit)
                {
                    if (Info.ProjectedMap.preyMap[current.Coord.X, current.Coord.Y] < 0 || Info.closeAllyEntities[entity.Id].Length > 5 && Info.ProjectedMap.dangerMap[current.Coord.X, current.Coord.Y] < 0)
                    {
                        var cellIsEmpty = Info.ProjectedMap.map[current.Coord.X, current.Coord.Y].Entity.Id == 0;
                        var cellIsResource = Info.ProjectedMap.map[current.Coord.X, current.Coord.Y].Entity.EntityType == EntityType.Resource;
                        var cellIsWall = Info.ProjectedMap.map[current.Coord.X, current.Coord.Y].Entity.EntityType == EntityType.Wall;
                        var cellIsMyself = Info.ProjectedMap.map[current.Coord.X, current.Coord.Y].Entity.Id == entity.Id;
                        if (cellIsEmpty || cellIsResource || cellIsWall || cellIsMyself)
                        {
                            closest = current.Coord;
                            break;
                        }
                    }
                }
                else if (entity.EntityType == EntityType.BuilderUnit)
                {
                    if (Info.ProjectedMap.builderDangerMap[current.Coord.X, current.Coord.Y] <= 0)
                    {
                        var currentCellEntity = Info.ProjectedMap.GetCell(current.Coord).Entity;
                        var currentCellProperties = Info.PlayerView.EntityProperties[currentCellEntity.EntityType];
                        var repairers = Info.BuildingsRepairersCount.ContainsKey(currentCellEntity.Id) ? Info.BuildingsRepairersCount[currentCellEntity.Id] : 0;
                        var goodResource = currentCellEntity.EntityType == EntityType.Resource && (Info.AvailableResources.Count == 0 || entity.Position.Dist(current.Coord) < 3 || Info.AvailableResources.Contains(current.Coord));
                        if (closestDist.More(0) && (Info.ProjectedMap.builderDangerMap[from.X, from.Y] > 0 ^ goodResource))
                        {
                            closestDist = 0;
                            closest = current.Coord;
                            if (Info.ProjectedMap.builderDangerMap[from.X, from.Y] > 0)
                            {
                                break;
                            }
                        }
                        if (currentCellEntity.Health < currentCellProperties.MaxHealth - Info.RepairThreshold - repairers * (current.G - 1))
                        {
                            if (currentCellEntity.EntityType.In(Info.EntityTypesForFastRepair))
                            {
                                closest = current.Coord;
                                break;
                            }
                            if (currentCellEntity.EntityType == EntityType.House && !currentCellEntity.Active && current.G < System.Math.Max(2, repairDist / 2) && Info.MyEntitiesByType[EntityType.RangedBase].Length == 0)
                            {
                                closest = current.Coord;
                                break;
                            }
                        }
                    }
                }
                foreach (var d in Info.Vec4)
                {
                    var pos = current.Coord.Plus(d);
                    if (Info.OutOfBounds(pos))
                    {
                        continue;
                    }
                    var cost = GetCost(entity, pos);
                    if (cost == int.MaxValue)
                    {
                        continue;
                    }
                    var g = current.G + cost;
                    if (g.Less(map[pos.X, pos.Y].G))
                    {
                        map[pos.X, pos.Y].Parent = current.Coord;
                        map[pos.X, pos.Y].G = g;
                        double dist = pos.Dist(to);
                        if (!Info.OutOfBounds(to))
                        {
                            if (System.Math.Abs(to.X - current.Coord.X) < System.Math.Abs(to.Y - current.Coord.Y) && d.Y != 0)
                            {
                                dist -= 0.01;
                            }
                            if (System.Math.Abs(to.Y - current.Coord.Y) < System.Math.Abs(to.X - current.Coord.X) && d.X != 0)
                            {
                                dist -= 0.01;
                            }
                        }
                        map[pos.X, pos.Y].F = g + dist;
                        if (!queue.Contains(map[pos.X, pos.Y]))
                        {
                            queue.Add(map[pos.X, pos.Y]);
                            changedCells.Add(pos);
                            if (dist.Less(closestDist))
                            {
                                closestDist = dist;
                                closest = pos;
                            }
                        }
                    }
                }
            }
            var firstStep = GetFirstStep(closest);
            foreach (var pos in changedCells)
            {
                map[pos.X, pos.Y] = new PathMapCell(pos, pos);
            }
            changedCells = new List<Vec2Int>();
            Info.timer[6].Stop();
            return firstStep;
        }
    }

    public class ProjectedMap : Map<MapCell>
    {
        public int[,] preyMap;
        public int[,] dangerMap;
        public int[,] builderDangerMap;
        public Dictionary<int, Entity> entitiesById;
        public Dictionary<int, Entity> enemyEntitiesById;
        public Dictionary<EntityType, Entity[]> enemyEntitiesByType;

        public ProjectedMap()
        {
            map = new MapCell[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            preyMap = new int[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            dangerMap = new int[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            builderDangerMap = new int[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            entitiesById = new Dictionary<int, Entity>();
            enemyEntitiesById = new Dictionary<int, Entity>();
            enemyEntitiesByType = new Dictionary<EntityType, Entity[]>();
        }

        public void Update()
        {
            entitiesById = new Dictionary<int, Entity>();
            enemyEntitiesById = new Dictionary<int, Entity>();
            for (int i = 0; i < Info.PlayerView.MapSize; i++)
            {
                for (int j = 0; j < Info.PlayerView.MapSize; j++)
                {
                    if (Info.OnceVisible.map[i, j].Visible)
                    {
                        map[i, j] = Info.OnceVisible.map[i, j];
                        if (map[i, j].Entity.EntityType != EntityType.Resource && !entitiesById.ContainsKey(map[i, j].Entity.Id))
                        {
                            entitiesById.Add(map[i, j].Entity.Id, map[i, j].Entity);
                            if (map[i, j].Entity.Id != 0 && map[i, j].Entity.PlayerId != Info.PlayerView.MyId)
                            {
                                enemyEntitiesById.Add(map[i, j].Entity.Id, map[i, j].Entity);
                            }
                        }
                    }
                    else if (Info.ResourceMap.map[i, j].Visible)
                    {
                        map[i, j] = Info.ResourceMap.map[i, j];
                    }
                    else
                    {
                        map[i, j] = new MapCell(new Entity(Info.GetId(), Info.GetId(), EntityType.Resource, new Vec2Int(i, j), Info.PlayerView.EntityProperties[EntityType.Resource].MaxHealth, true), false);
                    }
                }
            }
            enemyEntitiesByType = new Dictionary<EntityType, Entity[]>();
            foreach (EntityType type in System.Enum.GetValues(typeof(EntityType)))
            {
                enemyEntitiesByType[type] = enemyEntitiesById.Select(x => x.Value).Where(x => x.EntityType == type).ToArray();
            }
            UpdateEnemyMap();
        }

        private void UpdateEnemyMap()
        {
            preyMap = new int[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            dangerMap = new int[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            builderDangerMap = new int[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            var damage = Info.PlayerView.EntityProperties[EntityType.RangedUnit].Attack.Value.Damage;
            foreach (var entityIdValue in enemyEntitiesById)
            {
                var entity = entityIdValue.Value;
                var hits = (entity.Health + damage - 1) / damage;
                var gang = 0;
                if (!entity.Active)
                {
                    gang = 0;
                }
                else if (entity.EntityType == EntityType.RangedUnit)
                {
                    if (hits <= 1)
                    {
                        gang = 1;
                    }
                    else
                    {
                        gang = 2;
                    }
                }
                else if (entity.EntityType == EntityType.Turret)
                {
                    if (hits <= 2)
                    {
                        gang = 1;
                    }
                    else if (hits <= 5)
                    {
                        gang = 2;
                    }
                    else if (hits <= 8)
                    {
                        gang = 3;
                    }
                    else if (hits <= 13)
                    {
                        gang = 4;
                    }
                    else if (hits <= 18)
                    {
                        gang = 5;
                    }
                    else
                    {
                        gang = 6;
                    }
                }
                var range = gang > 0 ? Info.PlayerView.EntityProperties[entity.EntityType].Attack.Value.AttackRange : Info.PlayerView.EntityProperties[EntityType.RangedUnit].Attack.Value.AttackRange - 1;
                var points = Info.GetPointsInRange(entity, range + 1);
                var myUnitsAround = 0d;
                foreach (var pos in points)
                {
                    var dist = entity.Dist(pos);
                    if (dist <= range + 1 && map[pos.X, pos.Y].Entity.PlayerId == Info.PlayerView.MyId && map[pos.X, pos.Y].Entity.EntityType == EntityType.RangedUnit)
                    {
                        myUnitsAround += (map[pos.X, pos.Y].Entity.Health + damage - 1) / damage / 2d;
                    }
                }
                var currentTypeMap = entity.EntityType == EntityType.Turret && myUnitsAround < gang ? dangerMap : preyMap;
                if (gang > 0 && myUnitsAround >= gang)
                {
                    range--;
                }
                foreach (var pos in points)
                {
                    var dist = entity.Dist(pos);
                    if (dist <= range)
                    {
                        if (currentTypeMap[pos.X, pos.Y] < 0)
                        {
                            currentTypeMap[pos.X, pos.Y] = 0;
                        }
                        currentTypeMap[pos.X, pos.Y]++;
                    }
                    else if (dist == range + 1)
                    {
                        if (currentTypeMap[pos.X, pos.Y] <= 0)
                        {
                            currentTypeMap[pos.X, pos.Y]--;
                        }
                    }
                }
                var builderRange = Info.PlayerView.EntityProperties[entity.EntityType].Attack.HasValue ? Info.PlayerView.EntityProperties[entity.EntityType].Attack.Value.AttackRange : 0;
                if (entity.EntityType == EntityType.BuilderUnit)
                {
                    builderRange = 0;
                }
                else if (builderRange != 0 && entity.EntityType != EntityType.Turret)
                {
                    builderRange += 2;
                }
                points = Info.GetPointsInRange(entity, builderRange);
                foreach (var pos in points)
                {
                    builderDangerMap[pos.X, pos.Y]++;
                }
            }
        }
    }

    public class ResourceMap
    {
        public MapCell[,] map;

        public ResourceMap()
        {
            map = new MapCell[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
        }

        public void Update()
        {
            for (int i = 0; i < Info.PlayerView.MapSize; i++)
            {
                for (int j = 0; j < Info.PlayerView.MapSize; j++)
                {
                    if (map[i, j].Visible)
                    {
                        continue;
                    }
                    if (Info.OnceVisible.map[i, j].Visible)
                    {
                        if (Info.OnceVisible.map[i, j].Entity.EntityType == EntityType.Resource)
                        {
                            map[i, j] = Info.OnceVisible.map[i, j];
                        }
                        else
                        {
                            map[i, j] = new MapCell(new Entity(), true);
                        }
                        var pos = new Vec2Int(i, j);
                        if ((map[i, j].Entity.Id == 0 || map[i, j].Entity.EntityType == EntityType.Resource) && Info.MyTerritory(pos))
                        {
                            var points = Info.GetMirroredPoints(pos);
                            foreach (var p in points)
                            {
                                if (!map[p.X, p.Y].Visible)
                                {
                                    if (map[i, j].Entity.Id == 0)
                                    {
                                        map[p.X, p.Y] = new MapCell(new Entity(), true);
                                    }
                                    else
                                    {
                                        map[p.X, p.Y] = new MapCell(new Entity(Info.GetId(), Info.GetId(), map[pos.X, pos.Y].Entity.EntityType, p, Info.PlayerView.EntityProperties[EntityType.Resource].MaxHealth, true), false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public class NowVisible
    {
        public bool[,] map;

        public NowVisible()
        {
            map = new bool[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
        }

        public void Update()
        {
            map = new bool[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            foreach (var entity in Info.MyEntities)
            {
                var x = new Vec2Int(System.Math.Max(0, entity.Position.X - Info.PlayerView.EntityProperties[entity.EntityType].SightRange), System.Math.Min(Info.PlayerView.MapSize - 1, entity.Position.X + Info.PlayerView.EntityProperties[entity.EntityType].Size - 1 + Info.PlayerView.EntityProperties[entity.EntityType].SightRange));
                var y = new Vec2Int(System.Math.Max(0, entity.Position.Y - Info.PlayerView.EntityProperties[entity.EntityType].SightRange), System.Math.Min(Info.PlayerView.MapSize - 1, entity.Position.Y + Info.PlayerView.EntityProperties[entity.EntityType].Size - 1 + Info.PlayerView.EntityProperties[entity.EntityType].SightRange));
                for (int i = x.X; i <= x.Y; i++)
                {
                    for (int j = y.X; j <= y.Y; j++)
                    {
                        var pos = new Vec2Int(i, j);
                        if (!map[pos.X, pos.Y] && entity.Dist(pos) <= Info.PlayerView.EntityProperties[entity.EntityType].SightRange)
                        {
                            map[pos.X, pos.Y] = true;
                        }
                    }
                }
            }
        }
    }

    public class OnceVisible : Map<MapCell>
    {
        public HashSet<Vec2Int> edge, potentialEdge;

        public OnceVisible()
        {
            map = new MapCell[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            edge = new HashSet<Vec2Int>();
            potentialEdge = new HashSet<Vec2Int>();
        }

        private void UpdatePotentialEdge()
        {
            potentialEdge = new HashSet<Vec2Int>();
            foreach (Vec2Int pos in edge)
            {
                foreach (var d in Info.Vec4)
                {
                    var posNew = pos.Plus(d);
                    if (!Info.OutOfBounds(posNew) && !map[posNew.X, posNew.Y].Visible)
                    {
                        potentialEdge.Add(posNew);
                    }
                }
            }
        }

        private bool UpdateEdge()
        {
            var result = false;
            foreach (var pos in potentialEdge)
            {
                if (map[pos.X, pos.Y].Entity.Id > 0 && Info.Visible(map[pos.X, pos.Y].Entity) || Info.Visible(pos))
                {
                    edge.Add(pos);
                    if (!map[pos.X, pos.Y].Visible)
                    {
                        map[pos.X, pos.Y] = new MapCell(new Entity(), true);
                    }
                    result = true;
                }
            }
            edge.RemoveWhere(x => {
                foreach (var d in Info.Vec4)
                {
                    var pos = x.Plus(d);
                    if (!Info.OutOfBounds(pos) && !map[pos.X, pos.Y].Visible)
                    {
                        return false;
                    }
                }
                return true;
            });
            UpdatePotentialEdge();
            return result;
        }

        public void Update()
        {
            var mapNew = new MapCell[Info.PlayerView.MapSize, Info.PlayerView.MapSize];
            foreach (var entity in Info.PlayerView.Entities)
            {
                for (int i = 0; i < Info.PlayerView.EntityProperties[entity.EntityType].Size; i++)
                {
                    for (int j = 0; j < Info.PlayerView.EntityProperties[entity.EntityType].Size; j++)
                    {
                        var pos = new Vec2Int(entity.Position.X + i, entity.Position.Y + j);
                        mapNew[pos.X, pos.Y] = new MapCell(entity, true);
                        if (!map[pos.X, pos.Y].Visible)
                        {
                            potentialEdge.Add(pos);
                        }
                    }
                }
            }
            for (int i = 0; i < Info.PlayerView.MapSize; i++)
            {
                for (int j = 0; j < Info.PlayerView.MapSize; j++)
                {
                    if (!mapNew[i, j].Visible && map[i, j].Visible)
                    {
                        if (map[i, j].Entity.PlayerId == Info.PlayerView.MyId)
                        {
                            mapNew[i, j] = new MapCell(new Entity(), true);
                            continue;
                        }
                        var entityExists = Info.EntitiesIds.Contains(map[i, j].Entity.Id);
                        if (entityExists)
                        {
                            mapNew[i, j] = new MapCell(new Entity(), true);
                            continue;
                        }
                        if (map[i, j].Entity.Id > 0 && Info.Visible(map[i, j].Entity) || Info.Visible(new Vec2Int(i, j)))
                        {
                            mapNew[i, j] = new MapCell(new Entity(), true);
                        }
                        else
                        {
                            mapNew[i, j] = map[i, j];
                        }
                    }
                }
            }
            map = mapNew;

            if (edge.Count == 0)
            {
                for (int i = 0; i < Info.PlayerView.MapSize; i++)
                {
                    for (int j = 0; j < Info.PlayerView.MapSize; j++)
                    {
                        var pos = new Vec2Int(i, j);
                        if (Info.Visible(pos))
                        {
                            edge.Add(pos);
                            if (!map[i, j].Visible)
                            {
                                map[i, j] = new MapCell(new Entity(), true);
                            }
                        }
                    }
                }
                UpdatePotentialEdge();
                while (UpdateEdge());
            }

            while (UpdateEdge());
            UpdateGhostEnemies();
        }

        private void UpdateGhostEnemies()
        {
            if (Info.Round == 1)
            {
                return;
            }
            if (Info.MyEntitiesByType[EntityType.RangedBase].Length > 0)
            {
                var entity = new Entity(Info.GetId(), Info.GetId(), EntityType.MeleeUnit, Info.VecZero, 1, true);
                if (Info.Round == 2)
                {
                    var myPos = Info.MyEntitiesByType[EntityType.RangedBase][0].Position;
                    var d = Info.PlayerView.MapSize - 1 - System.Math.Max(myPos.X, myPos.Y) - 1;
                    var ghostCorrection = Info.PlayerView.CurrentTick / 10 % 2 == 0 ? 1 : -1;

                    var pos1 = new Vec2Int(myPos.X, System.Math.Clamp(myPos.Y + d + ghostCorrection, 0, Info.PlayerView.MapSize - 1));
                    if (GetCell(pos1).Entity.EntityType == EntityType.MeleeUnit)
                    {
                        map[pos1.X, pos1.Y] = new MapCell(new Entity(), false);
                    }
                    var pos2 = new Vec2Int(myPos.X, System.Math.Clamp(myPos.Y + d - ghostCorrection, 0, Info.PlayerView.MapSize - 1));
                    if (GetCell(pos2).Entity.EntityType == EntityType.MeleeUnit)
                    {
                        map[pos2.X, pos2.Y] = new MapCell(new Entity(), false);
                    }
                    entity.Position = pos1;
                    if (!GetCell(entity.Position).Visible)
                    {
                        map[entity.Position.X, entity.Position.Y] = new MapCell(entity, true);
                    }

                    pos2 = new Vec2Int(System.Math.Clamp(myPos.X + d + ghostCorrection, 0, Info.PlayerView.MapSize - 1), myPos.Y);
                    if (GetCell(pos2).Entity.EntityType == EntityType.MeleeUnit)
                    {
                        map[pos2.X, pos2.Y] = new MapCell(new Entity(), false);
                    }
                    entity.Id = Info.GetId();
                    entity.Position = new Vec2Int(System.Math.Clamp(myPos.X + d - ghostCorrection, 0, Info.PlayerView.MapSize - 1), myPos.Y);
                    if (GetCell(entity.Position).Entity.EntityType == EntityType.MeleeUnit)
                    {
                        map[pos2.X, pos2.Y] = new MapCell(new Entity(), false);
                    }
                    if (!GetCell(entity.Position).Visible)
                    {
                        map[entity.Position.X, entity.Position.Y] = new MapCell(entity, true);
                    }
                }

                entity.Id = Info.GetId();
                entity.Position = new Vec2Int(Info.PlayerView.MapSize - 1, Info.PlayerView.MapSize - 1);
                if (!GetCell(entity.Position).Visible)
                {
                    map[entity.Position.X, entity.Position.Y] = new MapCell(entity, true);
                }

                entity.Id = Info.GetId();
                entity.Position = new Vec2Int(Info.Round == 2 ? 0 : Info.PlayerView.MapSize / 2, Info.PlayerView.MapSize - 1);
                if (!GetCell(entity.Position).Visible)
                {
                    map[entity.Position.X, entity.Position.Y] = new MapCell(entity, true);
                }

                entity.Id = Info.GetId();
                entity.Position = new Vec2Int(Info.PlayerView.MapSize - 1, Info.Round == 2 ? 0 : Info.PlayerView.MapSize / 2);
                if (!GetCell(entity.Position).Visible)
                {
                    map[entity.Position.X, entity.Position.Y] = new MapCell(entity, true);
                }
            }
        }
    }
}